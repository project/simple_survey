<?php

/**
 * @file
 * Contains survey_response.page.inc.
 *
 * Page callback for Survey Response entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Survey Response templates.
 *
 * Default template: survey_response.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_survey_response(array &$variables) {
  // Fetch SurveyResponse Entity Object.
  $survey_response = $variables['elements']['#survey_response'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
