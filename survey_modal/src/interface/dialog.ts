export default interface Dialog {
  display(parent: string): void;
}