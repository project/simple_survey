export interface QuestionInterface {
  active: boolean;
  startingQuestion: boolean;
  inputType: string;
  questionType: string;
  displayAs: string;
  disclaimerText: any;
  id: string;
  lastQuestion: boolean;
  text: any;
  dependencies: any;
  selectOptions?: any;
  options?: any;
  placeholder?: any;
  closeTimer?: boolean;
}
