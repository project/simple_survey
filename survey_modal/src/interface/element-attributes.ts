export interface ElementAttributes {
  [propName: string]: any;
}