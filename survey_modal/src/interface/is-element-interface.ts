import {ElementAttributes} from "./";

export interface IsElementInterface {
  domElement: HTMLElement;

  create(tagName: string): void;
  addAttribute(name: string, value: string): void;
  addAttributes(attributes: ElementAttributes): void;
  setEventListener(type: string, listener: EventListener): void;
  addToBody(): void;
  addToParentElementByTag(parentTag: string): void;
  addChildElement(childElement: HTMLElement|Text): void;
  removeElement(): void;
  addSelectOption(option: any): void;
  addOption(option: any, type: string, totalOptions: number, optionIndex: number, displayAs?: string): void;
  setHTML(text: string): void;
}