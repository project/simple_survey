import {QuestionInterface} from "./";

export interface QuestionsInterface {
  [propname:string]: QuestionInterface;
}