import {simpleSessionStorage} from "simple-storage";

export class DataStorageService {
  private db = simpleSessionStorage;
  private prefix = 'simple_survey.'
  private paramKeys: Array<string> = [
    'site_path',
    'language',
    'uuid'
  ];

  itemIsNull(key: string) {
    return this.db.getItem(this.prefix + key) === null;
  }

  getItem(key: string) {
    return this.db.getItem(this.prefix + key);
  }

  setItem(key: string, value: string) {
    this.db.setItem(this.prefix + key, value);
  }

  removeItem(key: string) {
    this.db.removeItem(this.prefix + key);
  }

  addParamKey(key: string) {
    if (this.paramKeys.indexOf(key) === -1) {
      this.paramKeys.push(key);
    }
  }

  getParams() {
    let params: Array<string | number> = [];

    for(let key of this.paramKeys){
      params.push(key + '=' + this.db.getItem(this.prefix + key));
    }

    return params;
  }

  removeParams() {
    for(let param in this.paramKeys) {
      this.removeItem(param);
    }
  }
}
