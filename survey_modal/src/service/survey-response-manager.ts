import { simpleLocalStorage } from "simple-storage";
import { DataStorageService } from "./dataStorage.service";
import { Config, SurveyResponses, SurveyResponse } from "@model";

export class SurveyResponseManager {
  private readonly config: Config;
  private localStorage = simpleLocalStorage;
  private sessionStorage: DataStorageService;
  private surveyResponses: SurveyResponses = {};
  private localStorageName = "simple_survey_responses";
  private currentSitePath: string;
  private currentResponse: SurveyResponse;
  private surveyUsername = "simple_survey_user";
  private pauseSurveyKey = "simple_survey_pause";

  constructor(config: Config) {
    this.config = config;
    this.sessionStorage = new DataStorageService();
    this.currentResponse = new SurveyResponse();
    this.surveyResponses = this.getResponseManager();
    this.currentSitePath = this.sessionStorage.getItem("site_path");
    this.retrieveResponse();
  }

  setUuid(uuid: string) {
    this.localStorage.setItem(this.surveyUsername, uuid);
  }

  getUuid() {
    return this.localStorage.getItem(this.surveyUsername);
  }

  getPausedTime() {
    return this.localStorage.getItem(this.pauseSurveyKey) === null
      ? 0
      : this.localStorage.getItem(this.pauseSurveyKey);
  }

  pauseSurvey() {
    let currentTime = new Date().getTime();
    this.localStorage.setItem(
      this.pauseSurveyKey,
      this.config.displayCooldown + currentTime
    );
  }

  resetSurveyPause() {
    this.localStorage.setItem(this.pauseSurveyKey, 0);
  }

  surveyIsPaused() {
    let currentTime = new Date().getTime();
    let releaseTime = this.getPausedTime();
    let paused = releaseTime > currentTime;

    if (!paused) {
      this.resetSurveyPause();
    }

    return paused;
  }

  getResponseManager() {
    let ls = this.localStorage.getItem(this.localStorageName);

    if (ls === null) {
      this.localStorage.setItem(this.localStorageName, {});
      ls = {};
    }

    return ls;
  }

  createResponse() {
    this.currentResponse = new SurveyResponse();

    this.currentResponse.sitePath = this.currentSitePath;
    this.updateSurveyResponses();
  }

  retrieveResponse() {
    if (!this.surveyResponses.hasOwnProperty(this.currentSitePath)) {
      this.createResponse();
    } else {
      this.currentResponse = this.surveyResponses[this.currentSitePath];
    }
  }

  updateSurveyResponses() {
    this.surveyResponses[this.currentSitePath] = this.currentResponse;
    this.localStorage.setItem(
      this.localStorageName,
      JSON.stringify(this.surveyResponses)
    );
  }

  responseSubmitted() {
    let currentTime = new Date().getTime();
    if (this.currentResponse !== undefined) {
      this.currentResponse.submitted = currentTime;
      this.currentResponse.maxAge = this.config.answeredDisplayCooldown;
      this.updateSurveyResponses();
    }
  }

  isSubmitted() {
    let submitted = false;

    if (
      this.currentResponse.submitted === null ||
      this.currentResponse.submitted === undefined
    ) {
      return submitted;
    }

    submitted = this.currentResponse.submitted > 0;

    return submitted;
  }

  hideSurvey() {
    let hideSurvey = this.surveyIsPaused();

    if (!hideSurvey) {
      if (this.isSubmitted()) {
        let currentTime = new Date().getTime();
        let expires =
          this.currentResponse.submitted + this.currentResponse.maxAge;
        hideSurvey = expires > currentTime;
      }

      if (!hideSurvey) {
        this.resetResponse();
      }
    }

    return hideSurvey;
  }

  resetResponse() {
    this.currentResponse.submitted = 0;
    this.currentResponse.maxAge = 0;
    this.updateSurveyResponses();
  }
}
