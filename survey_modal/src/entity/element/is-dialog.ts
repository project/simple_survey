import { IsElement } from "./is-element";
import {Dialog, ElementAttributes, IsElementInterface} from "../../interface";
import { Config } from "@model";
import { SurveyResponseManager } from "@service";

/***
 * Controls the main dialog container
 *
 * @property isElement class that manages the element configuration
 * @property domElement the element loaded into the DOM
 */
export class IsDialog extends IsElement implements Dialog {

  //@inject('IsElement') @named('CloseButton') private closeButton: IsElementInterface;
  private closeButton: IsElement;
  private staticDialog: boolean;

  /**
   * Generates the DOM element on instantiation of the class
   *
   * @param tagName the tag that should be used to generate the element
   * @param attributes the attributes of the element
   * @param staticDialog marks dialog as being sticky or static. Defaults to sticky
   */
  constructor(tagName: string, config:Config, attributes: ElementAttributes, staticDialog: boolean = false) {
    super(tagName, config);

    this.staticDialog = staticDialog;
    this.addAttributes(attributes);
    this.closeButton = new IsElement("button");

    if(!staticDialog) {
      this.addCloseButton();
    }

    this.setEventListener('isfComplete',()=> {
      setTimeout(() => {
        this.removeElement();
      }, 7000);
    })
  }

  /**
   * Adds the Element to the DOM
   *
   * @param parent the parent that the element should be appended to.
   */
  display(parent: string) {
    if(parent === 'body') {
      this.addToBody();
    }
    else {
      let tag = parent.replace(/[#.]/,'');
      this.domElement.classList.add('static-survey');

      if(parent.search(/#/) > -1) {
        this.addToParentElementByTag(tag);
      } else {
        this.addToParentElementByClassName(tag);
      }
    }
  }

  /**
   * Adds a close button to the dialog
   */
  private addCloseButton() {
    this.closeButton = new IsElement('button');
    let faelement = new IsElement('i');
    let srm = new SurveyResponseManager(this.config);

    this.closeButton.addAttributes( {
      'class': 'isf--close',
      'id': 'isfClose',
      'aria-label': 'Close'
    });

    faelement.addAttributes({
      'class': 'far fa-window-close'
    });

    this.closeButton.addChildElement(faelement.domElement);

    this.closeButton.setEventListener('click',() => {
      srm.pauseSurvey();
      this.removeElement();
    });

    this.addChildElement(this.closeButton.domElement);
  }

}
