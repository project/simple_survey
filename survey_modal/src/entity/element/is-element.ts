import { IsElementInterface, ElementAttributes } from "../../interface";
import { DataStorageService } from "@service";
import { Config } from "@model";

export class IsElement implements IsElementInterface {
  private db: DataStorageService;
  private language: string;

  domElement: HTMLElement;
  config: Config | null;
  submitCallback: any;

  constructor(tagName:string, config:Config = null) {
    this.db = new DataStorageService();
    this.config = config;
    this.language = this.db.getItem('language');
    this.create(tagName);
  }

  create(tagName: string) {
    this.domElement = document.createElement(tagName);
  }

  addAttribute(name: string, value: string) {
    this.domElement.setAttribute(name, value);
  }

  addAttributes(attributes: ElementAttributes) {
    for(let attribute in attributes) {
      if(attributes.hasOwnProperty(attribute)) {
        this.addAttribute(attribute, attributes[attribute]);
      }
    }
  }

  setEventListener(type: string, listener: EventListener) {
    this.domElement.addEventListener(type, listener);
  }

  addToBody() {
    document.body.appendChild(this.domElement);
  }

  addToParentElementByTag(parentTag: string) {
    let parent = document.getElementById(parentTag);
    if (parent !== null) {
      parent.appendChild(this.domElement);
    }
  }

  addToParentElementByClassName(classTag: string) {
    document.getElementsByClassName(classTag)[0].appendChild(this.domElement);
  }

  addChildElement(childElement: HTMLElement|Text) {
    this.domElement.appendChild(childElement);
  }

  removeElement() {
    let parent = this.domElement.parentNode;

    if (parent !== null)
      parent.removeChild(this.domElement);
  }

  addSelectOption(option: any) {
    let selectOption: HTMLOptionElement;

    selectOption = document.createElement('option');
    selectOption.value = selectOption.textContent = option;

    this.addChildElement(selectOption);
  }

  /**
   * @todo This is doing WAY too much. Need to address at a later date - KLR
   * @param option the option element
   * @param type the type of option radio or checkbox
   * @param totalOptions total options being displayed
   * @param optionIndex the current options index
   * @param displayAs if this should be displayed as starts or as a button
   */
  addOption(
    option: any,
    type: string,
    totalOptions: number,
    optionIndex: number,
    questionId: string,
    displayAs?: string) {
    let inputGroupInputLabel = new IsElement('label');
    let inputGroupInput = new IsElement('input');
    let faDisplayContainer = new IsElement('span');
    let faDisplayActiveContainer = new IsElement('span');
    let faDisplay = new IsElement('i');
    let faDisplayActive = new IsElement('i');
    let labelText = new IsElement('span');
    let labelId = option.name + 'label' + optionIndex;
    let starValue = 0;

    labelText.setHTML(option.text[this.language]);

    inputGroupInputLabel.addAttributes({
      'for': option.id,
      'id': labelId,
      'class': 'isf--' + type + (displayAs === undefined ? '' : ' isf--' + displayAs)
    });

    inputGroupInput.addAttributes({
      'type': displayAs === 'buttons' ? 'hidden' : type,
      'id': option.id,
      'name': type === 'radio' ? questionId : questionId + '[]',
      'value': option.value
    });

    if (displayAs === 'stars') {
      inputGroupInputLabel.addChildElement(inputGroupInput.domElement);
      faDisplayContainer.addChildElement(faDisplay.domElement);
      faDisplayActiveContainer.addChildElement(faDisplayActive.domElement);
      faDisplayActiveContainer.addAttribute('class', 'active');
      faDisplayContainer.addAttribute('class', 'inactive');
      faDisplay.addAttribute('class', 'far fa-star');
      faDisplayActive.addAttribute('class', 'fas fa-star');
      inputGroupInputLabel.addChildElement(faDisplayContainer.domElement);
      inputGroupInputLabel.addChildElement(faDisplayActiveContainer.domElement);

      inputGroupInputLabel.setEventListener('mouseover', () => {
        starValue = this.db.getItem(option.name) - 1;
        for(let i = 0; i <= optionIndex; i++) {
          document.getElementById(option.name + 'label' + i).classList.add('hover');

          if(i > starValue) {
            document.getElementById(option.name + 'label' + i).classList.add('active');
            document.getElementById(option.name + 'label' + i).classList.remove('inactive');
          }
        }
      });

      inputGroupInputLabel.setEventListener('mouseout', () => {
        starValue = this.db.getItem(option.name) - 1;

        for(let i = 0; i <= optionIndex; i++) {
          document.getElementById(option.name + 'label' + i).classList.remove('hover');

          if(i > starValue) {
            document.getElementById(option.name + 'label' + i).classList.add('inactive');
            document.getElementById(option.name + 'label' + i).classList.remove('active');
          }
        }
      });

      this.addChildElement(inputGroupInputLabel.domElement);
    }
    else if (displayAs === 'buttons') {
      inputGroupInputLabel.addAttribute('tabindex', "0");
      inputGroupInputLabel.addChildElement(labelText.domElement);

      this.addChildElement(inputGroupInput.domElement);
      this.addChildElement(inputGroupInputLabel.domElement);
    }
    else {
      inputGroupInput.addAttribute('tabindex', "0");
      inputGroupInputLabel.addChildElement(inputGroupInput.domElement);
      inputGroupInputLabel.addChildElement(labelText.domElement);

      this.addChildElement(inputGroupInputLabel.domElement);
    }

    this.addChildElement(inputGroupInputLabel.domElement);

    if(type === 'checkbox') {
      return inputGroupInput;
    }
    else {
      if (displayAs === 'buttons') {
        return inputGroupInputLabel;
      }
      else {
        return inputGroupInput;
      }
    }
  };

  setHTML(text: string) {
    this.domElement.innerHTML = text;
  }

  submitInput(input: any) {
    this.db.setItem(input.name, input.value);
  }

  submitCheck(input: any) {
    let checkArray: Array<string> = [];
    let inputName = input.domElement.name.toString().replace(/\[\]/, '');

    if(!this.db.itemIsNull(inputName)) {
      checkArray = (this.db.getItem(inputName).length > 1) ?
        this.db.getItem(inputName).split('|') : [this.db.getItem(inputName)];
    }

    let valueIndex = checkArray.indexOf(input.domElement.value);

    if(valueIndex > -1) {
      if(checkArray.length === 1) {
        checkArray = [];
      } else {
        checkArray.splice(checkArray.indexOf(input.domElement.value),1);
      }
    }
    else {
      checkArray.push(input.domElement.value);
    }

    this.db.setItem(inputName, checkArray.join('|'));
  }
}
