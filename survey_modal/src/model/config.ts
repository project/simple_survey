export class Config {
  parentTag: string = 'body';
  containerTag: string = 'div';
  attachStylesheet: boolean = true;
  path: string;
  spanishTranslation: boolean = false;
  debug: boolean = true;
  displayDelayType: string = 'time';
  configDelayPosition: string = 'bottom';
  delay: number = 0;
  basePath: string;
  baseUrl: string;
  questionsPath: string;
  submitPath: string;
  validationPath: string;
  closeDelay: number = 7;
  useFontAwesome: boolean = true;
  ombId: string;
  ombExpiration: string;
  ombDisclaimer: boolean = true;
  ombLink: string;
  modulePath: string;
  startingQuestion: string;
  survey: string;
  /**
   * @var {Number} displayCooldown
   *   The amount of time in days before survey will display after being closed.
   */
  displayCooldown: number;
  /**
   * @var {Number} answeredDisplayCooldown
   *   The amount of time in days before survey will display after being answered.
   */
  answeredDisplayCooldown: number;

  constructor(config: Config) {
    let defaultDisplayCooldown = 1000 * 60 * 60 * 24;

    this.parentTag = config.parentTag === undefined?'body':config.parentTag;
    this.containerTag = config.containerTag === undefined?'div':config.containerTag;
    this.attachStylesheet = config.attachStylesheet === undefined?true:config.attachStylesheet;
    this.path = config.path === undefined?'.':config.path;
    this.spanishTranslation = config.spanishTranslation === undefined?false:config.spanishTranslation;
    this.displayDelayType = config.displayDelayType === undefined?'time':config.displayDelayType;
    this.configDelayPosition = config.configDelayPosition === undefined?'bottom':config.configDelayPosition;
    this.debug = config.debug === undefined?false:config.debug;
    this.delay = config.delay === undefined ? 0 : config.delay * 1000;
    this.closeDelay = config.closeDelay === undefined?7:config.closeDelay;
    this.basePath = config.basePath === undefined?'':config.basePath;
    this.submitPath = config.submitPath === undefined?'':config.submitPath;
    this.validationPath = config.validationPath === undefined?'':config.validationPath;
    this.useFontAwesome = config.useFontAwesome === undefined?false:config.useFontAwesome;
    this.ombId = config.ombId === undefined?'XXXXXX':config.ombId;
    this.ombExpiration = config.ombExpiration === undefined?'XX/XX/XXXX':config.ombExpiration;
    this.ombDisclaimer = config.ombDisclaimer === undefined?true:config.ombDisclaimer;
    this.ombLink = config.ombLink === undefined?'':config.ombLink;
    this.modulePath = config.modulePath === undefined ? "" : config.modulePath;
    this.startingQuestion = config.startingQuestion === undefined ? "" : config.startingQuestion;
    this.survey = config.survey === undefined ? "default" : config.survey;
    this.displayCooldown = config.displayCooldown === undefined ? defaultDisplayCooldown : config.displayCooldown * defaultDisplayCooldown;
    this.answeredDisplayCooldown = config.answeredDisplayCooldown === undefined ? this.displayCooldown : config.answeredDisplayCooldown * defaultDisplayCooldown;

    this.questionsPath = config.questionsPath === undefined ? '/questions.json' : config.questionsPath;
    this.setDebugValues()
  }

  private setDebugValues() {
    if(this.debug){
      console.log('In debug mode');
      this.delay = 0;

    }
  }
}
