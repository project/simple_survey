import { SurveyResponse } from "./survey-response";

export class SurveyResponses {
  [propname: string]: SurveyResponse;
}