import { simpleLocalStorage } from "simple-storage";

export class SurveyResponse {
  submitted = 0;
  maxAge = 0;
  sitePath = '';
}