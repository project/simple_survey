import { Config } from "../model/config";
import axios from "axios"
import {from} from "rxjs";
import {QuestionsInterface} from "../interface";
import { DataStorageService, SurveyResponseManager } from "@service";
import {IsDialog, IsElement} from "../entity";
import { QuestionElement } from "./question-element";

export class Questions {
  private uuid: string | number;
  private http = axios;
  private questionsContainer: IsDialog;
  private db: DataStorageService;
  private srm: SurveyResponseManager;

  public questions: QuestionsInterface;

  constructor(private config: Config) {
    this.db = new DataStorageService();
    this.srm = new SurveyResponseManager(config);

    if(this.config.debug) {
      this.uuid = Math.random();
      this.getQuestions().subscribe(
        this.setQuestions,
        () => {
          console.log("Question file did not load")
        }
      );
    }
    else {
      this.getQuestions().subscribe(this.setQuestions);
    }
  }

  getQuestions() {
    let path = this.config.questionsPath;

    return  from(this.http.get(path));
  }

  populateQuestions() {
    let textDisclaimer = {
      "question": ""
    }

    let textDisclaimerElement: IsElement;

    for(let question in this.questions) {
      let questionElement = new QuestionElement(this.questions[question], this.config.startingQuestion === question);

      if(this.questions[question].questionType === 'normal') {
        this.db.removeItem(this.questions[question].id);
        this.db.addParamKey(this.questions[question].id);
      }

      if (this.questions[question].disclaimerText !== undefined) {
        if (this.questions[question].disclaimerText.en) {
          textDisclaimer.question = question;
          textDisclaimerElement = new IsElement('div');
          textDisclaimerElement.setHTML(this.questions[question].disclaimerText.en);
          textDisclaimerElement.addAttribute('id', 'isfTextDisclaimer_' + question);
          let classList = ['isf--text-disclaimer'];

          if (this.questions[question].startingQuestion) {
            classList.push('active');
          }

          textDisclaimerElement.addAttribute('class', classList.join(' '));
        }
      }

      if(questionElement.domElement !== undefined) {
        this.questionsContainer.addChildElement(questionElement.domElement);
      }
    }

    if(this.config.ombDisclaimer) {
      let questionDisclaimer = new IsElement('div');
      let disclaimer = 'OMB#: ' + this.config.ombId + ' Form Approved,<br> Exp. Date: ' + this.config.ombExpiration;

      questionDisclaimer.addAttribute('class', 'isf--disclaimer');

      if(this.config.ombLink === '') {
        questionDisclaimer.setHTML(disclaimer);
      }
      else {
        let questionAnchor = new IsElement('a');
        questionAnchor.addAttributes({
          'href': this.config.ombLink,
          'target': '_blank'
        });

        questionAnchor.setHTML(disclaimer);
        questionDisclaimer.addChildElement(questionAnchor.domElement);
      }

      this.questionsContainer.addChildElement(questionDisclaimer.domElement);

      if (textDisclaimerElement !== undefined) {
        this.questionsContainer.addChildElement(textDisclaimerElement.domElement);
      }
    }
  }

  submitResponses = () => {
    let url = this.config.submitPath;
    let uuid = this.srm.getUuid();

    if (uuid !== null && uuid !== '') {
      this.db.setItem('uuid', uuid);
    }

    let paramString = this.db.getParams().join("&");

    from(this.http.post(url, paramString, {
      headers: {
        "Content-type": "application/x-www-form-urlencoded"
      }
    })).subscribe(res => {
      this.srm.setUuid(res.data);
      if (this.config.debug) {
        console.log(res.data);
      }
    }, error => {
        console.log(error);
    });

    this.srm.responseSubmitted();
    this.db.removeParams();

    let closeDelay = this.config.closeDelay - 1;
    let isfCtd = document.getElementById("isfCtd");

    let closeInterval = setInterval(() => {
      isfCtd.innerHTML = "Closing in " + closeDelay-- + " seconds";

      if (closeDelay === 0) {
        clearInterval(closeInterval);
      }
    }, 1000);
  }

  displayQuestions = () => {
    if(this.questionsContainer !== undefined) {
      return;
    }

    this.questionsContainer = new IsDialog(this.config.containerTag, this.config, {
      'class': 'isf--container local-override-class'
    }, this.config.parentTag !== 'body');

    this.questionsContainer.addAttribute('tabindex', '0');

    this.questionsContainer.display(this.config.parentTag);

    this.questionsContainer.setEventListener('isfComplete', this.submitResponses);

    this.populateQuestions();
  };

  setQuestions = (res: any) => {
    this.questions = res.data;

    if(res.data.length === 0) {
      return;
    }

    if(this.config.displayDelayType === "time") {
      setTimeout(this.displayQuestions, this.config.delay);
    }
    else if(this.config.displayDelayType === "position") {
      let scrollHeight = Math.max(
        document.body.scrollHeight, document.documentElement.scrollHeight,
        document.body.offsetHeight, document.documentElement.offsetHeight,
        document.body.clientHeight, document.documentElement.clientHeight
      );

      let alreadyAdded = false;

      if(scrollHeight > document.documentElement.clientHeight) {
        window.addEventListener('scroll',() => {
          let currentScroll = window.pageYOffset + document.documentElement.clientHeight + 200;

          if(currentScroll >= scrollHeight && !alreadyAdded) {
            this.displayQuestions();
            alreadyAdded = true;
          }
        })
      }
      else {
        this.displayQuestions();
      }

    }
    else {
      this.displayQuestions();
    }
  };
}
