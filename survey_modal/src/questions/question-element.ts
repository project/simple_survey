import {IsElement} from "@entity";
import { DataStorageService } from "@service";
import {QuestionInterface} from "../interface";

export class QuestionElement {
  private db: DataStorageService;
  private language: string;
  private nextQuestion: string;

  domElement: HTMLElement;
  question: QuestionInterface;
  questionForm: IsElement;
  questionFormContainer: IsElement;

  constructor(question: QuestionInterface, starting = false) {
    this.db = new DataStorageService();
    this.language = this.db.getItem('language');
    this.question = question;

    switch (this.question.inputType) {
      case "checkbox":
      case "radio":
        this.generateOptionForm();
        break;
      case "text":
        this.generateTextForm();
        break;
      case "textArea":
        this.generateTextareaForm();
        break;
      case "select":
        this.generateSelectForm();
        break;
      default:
        this.generateForm();
        break;
    }

    if(this.questionForm !== undefined) {
      this.generateForm();
    }
  }

  generateOptionForm() {
    this.questionForm = new IsElement('div');

    this.questionForm.addAttributes({
      'class': this.question.inputType === 'radio' ? 'isf--radio-group' : 'isf--form-check-group'
    });

    const type = this.question.inputType;
    const displayAs = this.question.displayAs;
    const totalOptions = Object.keys(this.question.options).length;

    let optionIndex = 0;

    for(let option of this.question.options) {
      let optionObject = this.questionForm.addOption(option, type, totalOptions, optionIndex, this.question.id, displayAs);

      if (type === 'checkbox') {
        optionObject.setEventListener('click', () => {
          this.questionForm.submitCheck(optionObject);
        });
      }
      else {
        if (displayAs === 'buttons') {
          optionObject.setEventListener('click', () => {
            for (let i = 0; i < totalOptions; i++) {
              document.getElementById(option.name + 'label' + i).classList.remove('active');
            }

            optionObject.domElement.classList.add('active');

            this.questionForm.submitInput((<HTMLInputElement>document.getElementById(option.id)));
            this.submitGroup();
          });
        }
        else {
          optionObject.setEventListener('click', () => {
            this.questionForm.submitInput((<HTMLInputElement>document.getElementById(option.id)));
          });
        }
      }
      optionIndex++;
    }
  }

  generateSelectForm() {
    this.questionForm = new IsElement('select');

    this.questionForm.addAttributes({
      'id':  this.question.id,
      'name': this.question.id
    });

    for(let option of this.question.options[this.language]) {
      this.questionForm.addSelectOption(option);
    }

    this.questionForm.setEventListener('change',() => {
      this.questionForm.submitInput((<HTMLSelectElement> document.getElementById(this.question.id)));
    });
  };

  generateTextForm() {
    this.questionForm = new IsElement('input');

    this.questionForm.addAttributes({
      'id':  this.question.id,
      'name':  this.question.id,
      'type':  'text',
      'placeholder': this.question.placeholder[this.language]
    });

    this.questionForm.setEventListener('saveInput', ()=> {
      this.db.setItem(this.question.id, (<HTMLInputElement>document.getElementById(this.question.id)).value)
    })
  };

  generateTextareaForm() {
    this.questionForm = new IsElement('textarea');

    this.questionForm.addAttributes({
      'id':  this.question.id,
      'name':  this.question.id,
      'placeholder': this.question.placeholder[this.language]
    });

    this.questionForm.setEventListener('saveTextarea', ()=> {
      this.db.setItem(this.question.id, (<HTMLInputElement>document.getElementById(this.question.id)).value)
    })
  };

  generateSubmitGroup() {
    let inputGroupButton = new IsElement('button');

    inputGroupButton.addAttributes({
      'name':  this.question.id,
      'aria-label': 'Submit'
    });

    if(this.language === 'es') {
      if (this.question.questionType === 'closing') {
        inputGroupButton.setHTML("Enviar");
      } else {
        inputGroupButton.setHTML("Enviar");
      }
    } else {
      if (this.question.questionType === "closing") {
        inputGroupButton.setHTML("Submit");
      } else {
        inputGroupButton.setHTML("Submit");
      }
    }

    this.questionFormContainer.addChildElement(inputGroupButton.domElement);

    inputGroupButton.setEventListener('click',() => {
      let event;

      if(typeof (Event) === 'function') {
        event = new Event('saveTextarea');
      }
      else {
        event = document.createEvent('Event');
        event.initEvent('saveTextarea',true, true);
      }

      this.questionForm.domElement.dispatchEvent(event);
      this.submitGroup();
    });
  }

  generateForm() {
    this.questionFormContainer = new IsElement('div');
    let inputGroupLabel = new IsElement('label');

    this.questionFormContainer.addAttributes({
      'class': 'isf--form' + (this.question.startingQuestion?' active':'')
        + ' isf--' + this.question.questionType,
      'id': this.question.id + '_container'
    });

    inputGroupLabel.addAttributes({
      'class': 'isf--label'
    });

    inputGroupLabel.setHTML(this.question.text[this.db.getItem('language')]);

    if(this.question.questionType === 'closing') {
      let closeTimerDisplay = new IsElement('div');
      closeTimerDisplay.setHTML("Closing in 7 seconds");

      closeTimerDisplay.addAttributes({
        'id': 'isfCtd',
        'class': 'isf--ctd'
      });

      this.questionFormContainer.addChildElement(closeTimerDisplay.domElement);
    }

    this.questionFormContainer.addChildElement(inputGroupLabel.domElement);

    if(this.question.questionType !== 'closing') {
      this.questionFormContainer.addChildElement(this.questionForm.domElement);

      if (this.question.displayAs !== 'buttons') {
        this.generateSubmitGroup();
      }
    }

    this.domElement = this.questionFormContainer.domElement;
  };

  extractDependency(dependency: any, value: string, defaultId: string) {
    let derivedDestinationId = defaultId;

    if (dependency['source'] !== undefined) {
      if (dependency['source']) {
        let sourceQuestionValue = this.db.getItem('sq' + dependency['source']) == value;
        derivedDestinationId = sourceQuestionValue ? dependency['destination'] : defaultId;
      }
      else {
        let savedValueArray = this.db.getItem(this.question.id).split('|');

        derivedDestinationId = savedValueArray.indexOf(value) >= 0
          ? dependency["destination"]
          : defaultId;
      }
    }
    else {
      derivedDestinationId =
        this.db.getItem(this.question.id) == value ? dependency : defaultId;
    }

    return derivedDestinationId;
  }

  setDependency() {
    let db = this.db;
    let savedValue: string = db.getItem(this.question.id);
    let nextContainerId = this.question.dependencies['next_question'] === undefined ? '' : this.question.dependencies['next_question'];

    for (let key in this.question.dependencies) {
      if (key != 'next_question') {
        switch (this.question.inputType) {
          case 'radio':
          case 'select':
            nextContainerId = this.extractDependency(this.question.dependencies[key], key, nextContainerId);
            break;
          case 'checkbox':
            let savedValueArray = savedValue === null ? [] : savedValue.split('|');

            for (let value of savedValueArray) {
              let dependency = this.question.dependencies[value];

              if (dependency !== undefined) {
                let derivedContainerId = this.extractDependency(
                  dependency,
                  value,
                  nextContainerId
                );

                if (derivedContainerId != nextContainerId) {
                  nextContainerId = derivedContainerId;
                  break;
                }
              }
            }
            break;
        }
      }
    }

    return nextContainerId;
  }

  submitGroup() {
    let previousContainer: HTMLElement;
    let nextContainer: HTMLElement;
    let question = this.question;
    let domElement = this.domElement;
    let nextContainerId = this.setDependency();

    previousContainer = document.getElementById(question.id+'_container');
    nextContainer = document.getElementById(nextContainerId + '_container');

    if(previousContainer) {
      previousContainer.classList.toggle('active');
      if (document.getElementById('isfTextDisclaimer_' + question.id))
        document.getElementById('isfTextDisclaimer_' + question.id).classList.toggle('active');
    }

    if(nextContainer) {
      nextContainer.classList.toggle('active');
      if (document.getElementById('isfTextDisclaimer_' + nextContainerId))
        document.getElementById('isfTextDisclaimer_' + nextContainerId).classList.toggle('active');
    }

    if (nextContainer.classList.contains('isf--closing')) {
      let event;

      if (typeof Event === "function") {
        event = new Event("isfComplete");
      } else {
        event = document.createEvent("Event");
        event.initEvent("isfComplete", true, true);
      }

      domElement.parentElement.dispatchEvent(event);
    }
  };
}
