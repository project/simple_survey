import { Config } from "@model";
import { IsElement } from "@entity";
import 'ts-polyfill/lib/es2015-promise';
import './scss/style.scss';
import {Questions} from "./questions/questions";
import { DataStorageService, SurveyResponseManager } from "@service";


export class InterceptSurvey {
  private readonly config: Config;
  private db: DataStorageService;
  private srm: SurveyResponseManager;

  language: string = 'en';

  constructor(config: Config) {
    this.config = new Config(config);
    this.db = new DataStorageService();

    this.db.setItem('site_path', window.location.href);
    this.db.setItem('language', this.language);

    this.srm = new SurveyResponseManager(this.config);

    if(this.config.spanishTranslation && this.db.getItem('site_path.').search(/\/es\//) > -1) {
      this.language = 'es';
    }

    this.attachStylesheet();
  }

  displayForm() {
    if (!this.srm.hideSurvey()) {
      let questions = new Questions(this.config);
    }
  }

  attachStylesheet() {
    if(this.config.attachStylesheet) {
      let stylesheet = new IsElement('link');

      stylesheet.addAttributes({
        'rel': 'stylesheet',
        'href': this.config.modulePath + '/dist/main.css'
      });

      stylesheet.addToBody();
    }
  };
}
