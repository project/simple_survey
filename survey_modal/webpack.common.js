const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require("path");
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
  entry: "./src/index.ts",
  plugins: [
    new CleanWebpackPlugin([path.resolve(__dirname, "..", "dist")], {
      root: path.resolve(__dirname, "..")
    }),
    new HtmlWebpackPlugin({
      title: "Intecerpt Survey",
      template: "index.html"
    })
  ],
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        exclude: /(node_modules|bower_components)/,
        loader: "babel-loader",
        options: { presets: ["@babel/env"] }
      },
      {
        test: /\.(png|svg|jp(e*)g|gif)$/,
        use: [
          {
            loader: "url-loader",
            options: {
              limit: 8000,
              name: "images/[hash]-[name].[ext]"
            }
          }
        ]
      }
    ]
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"]
  }
};
