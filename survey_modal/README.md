# Intercept Survey Form
### Updating Questions
All questions are in the `questions.json` file. Each question has a unique
identifier which also serves as a key for the script to identify the question.

#### Properties
Each question has properties. They are as follows

`active`: Tells the script which question should be active. This has a boolean value.
The first question to display is true by default and only one question should have
be active set to true at any given time.

`inputType`: The type of form control that should be used. The options are: 
* select - creates a select dropdown
* textarea - creates a multiline text area
* radio - creates a yes/no radio

`id`: This is the on page id of the form control. It should be the same as the
unique identifier.

`text`: The text that will display for each question. Text has an english and 
spanish output. Leave a value blank of it should not be used.
* `es`: Spanish text output
* `en`: English text output

`dependencies`: This tells the form script what question should be displayed
after a user enters information. If the question is a radio it has the
script can go to a different question based on the option selected. This 
property uses the unique identifier to differentiate questions.

##### InputType Specific Properties

###### Textarea

`placeholder`: Adds place holder attribute to a textarea. Will only work on 
textareas. Like text this has english and spanish sub properties
* `es`: Spanish text output
* `en`: English text output

###### Radio

`options`: This adds the options for the radio property
* `id`: Unique id for each radio
* `name`: Form name property. ideally should be the same as the top level unique
identifier
* `value`: The value passed to the DB
* `class`: CSS class used to style the radio

###### Select

`selectOptions`: This adds the options for the select property.
* `en`: List all the english options for the select dropdown.
* `es`: List all the spanish options for the select dropdown