<?php

namespace Drupal\simple_survey\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Survey Response entities.
 */
class SurveyResponseViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();
    $baseTable = $this->entityType->getBaseTable() ?: $this->entityType->id();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    $data[$baseTable]['table']['join']['survey_answer'] = [
      'field' => 'uuid',
      'left_field' => 'parent_uuid',
      'type' => 'INNER',
    ];

    return $data;
  }

}
