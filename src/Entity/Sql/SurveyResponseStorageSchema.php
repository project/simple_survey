<?php

namespace Drupal\simple_survey\Entity\Sql;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines Survey Response storage schema.
 */
class SurveyResponseStorageSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritDoc}
   */
  protected function getEntitySchema(ContentEntityTypeInterface $entity_type, $reset = FALSE) {
    $schema = parent::getEntitySchema($entity_type, $reset);
    $dataTable = $this->storage->getDataTable();

    if ($dataTable) {
      $schema[$dataTable]['indexes'] += [
        'sssr_responses' => [
          'survey',
          'sent_to_bq',
          'created',
        ],
      ];
    }

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  protected function getSharedTableFieldSchema(FieldStorageDefinitionInterface $storage_definition, $table_name, array $column_mapping) {
    $schema = parent::getSharedTableFieldSchema($storage_definition, $table_name, $column_mapping);
    $field_name = $storage_definition->getName();

    if ($table_name == 'survey_response') {
      switch ($field_name) {
        case 'survey':
        case 'created':
        case 'sent_to_bq':
        case 'cookie_uuid':
          $this->addSharedTableFieldIndex($storage_definition, $schema, TRUE);
          break;
      }
    }

    return $schema;
  }

}
