<?php

namespace Drupal\simple_survey\Entity;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Query\QueryException;

/**
 * Provides a listing of Surveys.
 */
class SimpleSurveyListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Survey');
    $header['sqcount'] = $this->t('Number of Questions');
    $header['srcount'] = $this->t('Number of Responses');
    $header['bqsub'] = $this->t('Last BQ Submission');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\Core\Datetime\DateFormatter $dateFormatter */
    $dateFormatter = \Drupal::service('date.formatter');

    $surveyQuestionCount = \Drupal::service('entity_type.manager')->getStorage('survey_question')->getQuery()
      ->condition('survey', $entity->id())
      ->count()
      ->execute();

    try {
      $surveyResponseCount = \Drupal::service('entity_type.manager')->getStorage('survey_response')->getQuery()
        ->condition('survey', $entity->id())
        ->count()
        ->execute();
    }
    catch (QueryException $error) {
      $surveyResponseCount = 0;
    }

    $row['label'] = $entity->toLink();
    $row['sqcount'] = $surveyQuestionCount;
    $row['srcount'] = $entity->get('numberOfResponsesToStopAt') ? $this->t('%responses of %totalResponses', [
      '%responses' => $surveyResponseCount,
      '%totalResponses' => $entity->get('numberOfResponsesToStopAt'),
    ]) : $surveyResponseCount;
    $row['bqsub'] = !is_null($entity->getBqSubmission()) ?
      $dateFormatter->format($entity->getBqSubmission()) : 'Not Submitted to BigQuery';

    return $row + parent::buildRow($entity);
  }

}
