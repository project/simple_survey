<?php

namespace Drupal\simple_survey\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Survey Response entities.
 *
 * @ingroup simple_survey
 */
interface SurveyResponseInterface extends ContentEntityInterface, EntityChangedInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Survey Response name.
   *
   * @return string
   *   Name of the Survey Response.
   */
  public function getName();

  /**
   * Sets the Survey Response name.
   *
   * @param string $name
   *   The Survey Response name.
   *
   * @return \Drupal\simple_survey\Entity\SurveyResponseInterface
   *   The called Survey Response entity.
   */
  public function setName($name);

  /**
   * Gets the Survey Response creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Survey Response.
   */
  public function getCreatedTime();

  /**
   * Sets the Survey Response creation timestamp.
   *
   * @param int $timestamp
   *   The Survey Response creation timestamp.
   *
   * @return \Drupal\simple_survey\Entity\SurveyResponseInterface
   *   The called Survey Response entity.
   */
  public function setCreatedTime($timestamp);

}
