<?php

namespace Drupal\simple_survey\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Survey Response entity.
 *
 * @ingroup simple_survey
 *
 * @ContentEntityType(
 *   id = "survey_answer",
 *   label = @Translation("Survey Answer"),
 *   handlers = {
 *     "storage_schema" = "Drupal\simple_survey\Entity\Sql\SurveyAnswerStorageSchema",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *   },
 *   base_table = "survey_answer",
 *   admin_permission = "administer survey response entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "parent_uuid" = "parent_uuid",
 *   },
 *   links = {},
 * )
 */
class SurveyAnswer extends ContentEntityBase implements SurveyAnswerInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the SurveyAnswer entity.'))
      ->setReadOnly(TRUE);

    $fields['parent_uuid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the SurveyAnswer entity.'))
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 64,
        'text_processing' => 0,
      ));

    $fields['question_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Question ID'))
      ->setDescription(t('Question ID'))
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ));

    $fields['answer'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Answer'))
      ->setDescription(t('The survey response answer.'))
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 2083,
        'text_processing' => 0,
      ));

    return $fields;
  }

  public static function loadMultipleBatch($survey_uuid) {
    $db = \Drupal::database();

    $query = $db->select('survey_answer', 'sa')
      ->fields('sa')
      ->condition('sa.parent_uuid', $survey_uuid);

    return $query->execute()->fetchAll();
  }

}
