<?php

namespace Drupal\simple_survey\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a class to build a listing of Survey Response entities.
 *
 * @ingroup simple_survey
 */
class SurveyResponseListBuilder extends EntityListBuilder {

  /**
   * The survey the list belongs to.
   *
   * @var \Drupal\simple_survey\Entity\SimpleSurveyInterface
   */
  public $simpleSurvey;

  public $questions;

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage) {
    $this->entityTypeId = $entity_type->id();
    $this->storage = $storage;
    $this->entityType = $entity_type;
    $this->simpleSurvey = \Drupal::service('request_stack')->getCurrentRequest()->get('simple_survey');
    $surveyQuestionsIds = \Drupal::service('entity_type.manager')->getStorage('survey_question')->getQuery()
      ->condition('survey', $this->simpleSurvey->id())
      ->execute();

    $surveyQuestions = SurveyQuestion::loadMultiple($surveyQuestionsIds);

    $this->questions = [];

    foreach ($surveyQuestions as $surveyQuestion) {
      $this->questions[$surveyQuestion->id()] = $surveyQuestion->get('question')->value;
    }
  }

  /**
   * {@inheritDoc}
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->condition('survey', $this->simpleSurvey->id())
      ->sort('created', 'DESC');

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['submitted'] = $this->t('Submitted');
    $header['data'] = $this->t('Data');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\Core\Datetime\DateFormatter $dateFormatter */
    $dateFormatter = \Drupal::service('date.formatter');

    $row['id'] = $entity->id();
    $row['submitted'] = $dateFormatter->format($entity->get('created')->value);

    $surveyAnswerIds = \Drupal::service('entity_type.manager')->getStorage('survey_answer')->getQuery()
      ->condition('parent_uuid', $entity->uuid())
      ->execute();

    //$surveyAnswers = SurveyAnswer::loadMultiple($surveyAnswerIds);
    $row['data'] = '';

    foreach ($surveyAnswers as $surveyAnswer) {
      //$row['data'][] = $header[$surveyAnswer->get('question_id')->value] . '|' . $surveyAnswer->get('answer')->value;
    }

    return $row + parent::buildRow($entity);
  }

}
