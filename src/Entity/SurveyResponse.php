<?php

namespace Drupal\simple_survey\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Survey Response entity.
 *
 * @ingroup simple_survey
 *
 * @ContentEntityType(
 *   id = "survey_response",
 *   label = @Translation("Survey Response"),
 *   label_collection = @Translation("Survey responses"),
 *   label_singular = @Translation("Survey response"),
 *   label_plural = @Translation("Survey responses"),
 *   label_count = @PluralTranslation(
 *     singular = "@count survey response",
 *     plural = "@count survey responses",
 *   ),
 *   handlers = {
 *     "storage_schema" = "Drupal\simple_survey\Entity\Sql\SurveyAnswerStorageSchema",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\simple_survey\Entity\SurveyResponseListBuilder",
 *     "views_data" = "Drupal\simple_survey\Entity\SurveyResponseViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\simple_survey\Form\SurveyResponseForm",
 *       "delete" = "Drupal\simple_survey\Form\SurveyResponseDeleteForm",
 *     },
 *     "access" = "Drupal\simple_survey\SurveyResponseAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\simple_survey\Entity\Routing\SurveyResponseHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "survey_response",
 *   admin_permission = "administer survey response entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/simple_survey/{simple_survey}/response/{survey_response}",
 *     "delete-form" = "/admin/content/simple_survey/survey_response/{survey_response}/delete",
 *     "collection" = "/admin/content/simple_survey/{simple_survey}/responses",
 *   },
 *   field_ui_base_route = "entity.survey_response.collection"
 * )
 */
class SurveyResponse extends ContentEntityBase implements SurveyResponseInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the SurveyResponse entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the SurveyResponse entity.'))
      ->setReadOnly(TRUE);

    $fields['cookie_uuid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Cookie UUID'))
      ->setDescription(t('The user cookie'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 128,
        'text_processing' => 0,
      ]);

    $fields['site_path'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Site Path'))
      ->setDescription(t('URL the form was accessed from'))
      ->setSettings([
        'default_value' => '',
        'text_processing' => 0,
      ]);

    $fields['ip_address'] = BaseFieldDefinition::create('string')
      ->setLabel(t('IP Address'))
      ->setDescription(t('The first name of the SurveyResponse entity.'))
      ->setSettings([
        'default_value' => '',
        'text_processing' => 0,
      ]);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code of SurveyResponse entity.'));

    $fields['sent_to_bq'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Sent to BigQuery'))
      ->setDescription(t('Specifies if record was sent to BigQuery'))
      ->setDefaultValue(FALSE);

    $fields['survey'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Survey'))
      ->setDescription(t('Which surveys this response is for.'))
      ->setSetting('target_type', 'simple_survey')
      ->setSetting('handler', 'default:simple_survey');

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  public static function responseCountBatch($survey) {
    $db = \Drupal::database();

    $query = $db->select('survey_response', 'sr')
      ->condition('sr.survey', $survey);

    return $query->countQuery()->execute()->fetchField();
  }

  public static function loadMultipleDb($ids) {
    $db = \Drupal::database();

    $query = $db->select('survey_response', 'sr')
      ->fields('sr')
      ->condition('sr.id', $ids, 'IN');

    return $query->execute()->fetchAll();
  }

  public static function loadMultipleBatch($survey, $start = 0, $length = 50) {
    $db = \Drupal::database();

    $query = $db->select('survey_response', 'sr')
      ->fields('sr')
      ->condition('sr.survey', $survey)
      ->orderBy('sr.created', 'DESC')
      ->range($start, $length);

    return $query->execute()->fetchAll();
  }

}
