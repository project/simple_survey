<?php

namespace Drupal\simple_survey\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\simple_survey\Entity\SurveyQuestionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Survey Question definition.
 *
 * @ingroup simple_survey
 *
 * @ContentEntityType(
 *   id = "survey_question",
 *   label = @Translation("Survey Question"),
 *   label_collection = @Translation("Survey Questions"),
 *   label_singular = @Translation("Survey Question"),
 *   label_plural = @Translation("Survey Questions"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Survey Question",
 *     plural = "@count Survey Questions",
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\simple_survey\Form\SurveyQuestionForm",
 *       "edit" = "Drupal\simple_survey\Form\SurveyQuestionForm",
 *       "delete" = "Drupal\simple_survey\Form\SurveyQuestionDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "survey_question",
 *   admin_permission = "administer survey question entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "survey" = "survey"
 *   },
 *   links = {
 *     "canonical" = "/admin/content/simple_survey/question/{survey_question}",
 *     "edit-form" = "/admin/content/simple_survey/question/{survey_question}/edit",
 *     "delete-form" = "/admin/content/simple_survey/question/{survey_question}/delete",
 *     "collection" = "/admin/content/simple_survey/questions"
 *   }
 * )
 */
class SurveyQuestion extends ContentEntityBase implements SurveyQuestionInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * Builds a data schema from survey questions.
   *
   * @param string $survey
   *   The survey machine name.
   *
   * @return mixed
   *   Returns false if no questions. Return array of Questions/response type.
   */
  public static function buildSurveyTableSchema(string $survey) {
    $query = \Drupal::entityTypeManager()->getStorage('survey_question')
      ->getQuery()
      ->condition('survey', $survey);

    $questionIds = $query->execute();

    if (empty($questionIds)) {
      \Drupal::logger('simple_survey')->warning('No questions for survey ' . $survey);

      return FALSE;
    }

    $questions = self::loadMultiple($questionIds);
    $schemaData = [];

    foreach ($questions as $question) {
      if ($question->get('question_type')->value == 'normal') {
        $column = [];
        $sanitizeQuestion = strip_tags($question->get('question')->value);
        $sanitizeQuestion = str_replace('&nbsp;', ' ', $sanitizeQuestion);
        $sanitizeQuestion = str_replace(["\r", "\n"], '', $sanitizeQuestion);
        $column['description'] = $sanitizeQuestion;
        $sanitizeQuestion = str_replace(' ', '_', $sanitizeQuestion);
        $column['name'] = substr(preg_replace('/[^A-Za-z0-9_]/', '', strtolower($sanitizeQuestion)), 0, 128);
        $column['type'] = 'STRING';

        $schemaData[] = $column;
      }
    }

    return $schemaData;
  }

  /**
   * Builds a data schema from survey questions.
   *
   * @param string $survey
   *   The survey machine name.
   *
   * @return mixed
   *   Returns false if no questions. Return array of Questions/response type.
   */
  public static function generateQuestionMatrix(string $survey) {
    $query = \Drupal::entityTypeManager()->getStorage('survey_question')
      ->getQuery()
      ->condition('survey', $survey);

    $questionIds = $query->execute();

    if (empty($questionIds)) {
      \Drupal::logger('simple_survey')->warning('No questions for survey ' . $survey);

      return FALSE;
    }

    $questions = self::loadMultiple($questionIds);
    $matrix = [];

    foreach ($questions as $question) {
      if ($question->get('question_type')->value == 'normal') {
        $column = [];
        $sanitizeQuestion = strip_tags($question->get('question')->value);
        $sanitizeQuestion = str_replace('&nbsp;', ' ', $sanitizeQuestion);
        $sanitizeQuestion = str_replace(["\r", "\n"], '', $sanitizeQuestion);
        $column['label'] = $sanitizeQuestion;
        $sanitizeQuestion = str_replace(' ', '_', $sanitizeQuestion);
        $column['name'] = substr(preg_replace('/[^A-Za-z0-9_]/', '', strtolower($sanitizeQuestion)), 0, 128);

        $matrix[$question->id()] = $column;
      }
    }

    return $matrix;
  }

  /**
   * {@inheritDoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = [];

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Survey Question entity.'))
      ->setReadOnly(TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Survey Question entity.'))
      ->setReadOnly(TRUE);

    $fields['question'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Question'))
      ->setDescription(t('The question.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 500,
        'text_processing' => 0,
      ])
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'text_format',
        'weight' => 0,
      ]);

    $fields['question_disclaimer'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Question Disclaimer'))
      ->setDescription(t('A disclaimer or note relating to the question.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 500,
        'text_processing' => 0,
      ])
      ->setRequired(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'text_format',
        'weight' => 1,
      ]);

    $fields['question_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Question Type'))
      ->setDescription(t('Determines how a question will display:<ul><li>Question displays will have a submit/continue button.</li><li>Informational Displays will only have a continue button.</li><li>Closing Displays will display a timer to close.</li></ul>'))
      ->setSettings([
        'allowed_values' => [
          'normal' => 'Question display',
          'informational' => 'Informational display',
          'closing' => 'Closing display',
        ],
      ])
      ->setRequired(TRUE)
      ->setDefaultValue('text')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 1,
      ]);

    $fields['survey'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Survey'))
      ->setDescription(t('Which surveys this question appears on.'))
      ->setSetting('target_type', 'simple_survey')
      ->setSetting('handler', 'default:simple_survey')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 2,
      ]);

    $fields['next_question'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Default Next Question'))
      ->setDescription(t('The next question that should appear after this question is answered. Question branches override this question.'))
      ->setSetting('target_type', 'survey_question')
      ->setSetting('handler', 'views')
      ->setSetting('handler_settings', [
        'view' => [
          'view_name' => 'survey_question_entity_reference',
          'display_name' => 'entity_reference_1',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 3,
      ]);

    $fields['input_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Input Type'))
      ->setDescription(t('The type of input used.'))
      ->setSettings([
        'allowed_values' => [
          'text' => 'Text',
          'textArea' => 'Textarea',
          'checkbox' => 'Checkbox',
          'radio' => 'Radio',
          'select' => 'Dropdown',
          'none' => 'No Input Field',
        ],
      ])
      ->setRequired(TRUE)
      ->setDefaultValue('text')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 0,
      ]);

    $fields['display_as'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Display As'))
      ->setDescription(t('Determines how radio and checkboxes will appear.'))
      ->setDefaultValue('input')
      ->setSettings([
        'allowed_values' => [
          'input' => 'Normal',
          'stars' => 'Stars',
          'buttons' => 'Buttons',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 1,
      ]);

    $fields['placeholder_text'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Placeholder Text'))
      ->setDescription(t('If the input is a text area or text field, this text will be used as a place holder.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 2,
      ]);

    $fields['options'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Options'))
      ->setDescription(t('Options for select, radio, and checkbox input types'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 3,
      ])
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);

    $fields['branching'] = BaseFieldDefinition::create('branching_logic')
      ->setLabel(t('Question Branching'))
      ->setDescription(t('Here you can choose what question a user goes to if they choose a specific value for select, radio, and checkbox options. You can enter the option value and select the question you want to go to.'))
      ->setSetting('target_type', 'survey_question')
      ->setSetting('handler', 'views')
      ->setSetting('handler_settings', [
        'view' => [
          'view_name' => 'survey_question_entity_reference',
          'display_name' => 'entity_reference_1',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 3,
      ])
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code of Survey Question entity.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
