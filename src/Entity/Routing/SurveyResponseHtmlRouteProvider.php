<?php

namespace Drupal\simple_survey\Entity\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for Survey Response entities.
 *
 * @see \Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see \Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class SurveyResponseHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);
    $entity_type_id = $entity_type->id();

    return $collection;
  }

  /**
   * {@inheritDoc}
   */
  protected function getCanonicalRoute(EntityTypeInterface $entity_type) {
    $route = parent::getCanonicalRoute($entity_type);
    $entity_type_id = $entity_type->id();

    $route->setOption('parameters', [
      'simple_survey' => ['type' => 'entity:simple_survey'],
      $entity_type_id => ['type' => 'entity:' . $entity_type_id],
    ]);

    return $route;
  }

  /**
   * {@inheritDoc}
   */
  protected function getCollectionRoute(EntityTypeInterface $entity_type) {
    $route = parent::getCollectionRoute($entity_type);

    $route->setOption('parameters', [
      'simple_survey' => ['type' => 'entity:simple_survey'],
    ]);

    return $route;
  }

}
