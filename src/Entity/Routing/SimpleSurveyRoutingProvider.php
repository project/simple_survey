<?php

namespace Drupal\simple_survey\Entity\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides HTML routes for entities with administrative add/edit/delete pages.
 */
class SimpleSurveyRoutingProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritDoc}
   */
  protected function getCanonicalRoute(EntityTypeInterface $entity_type) {
    $entity_type_id = $entity_type->id();
    $route = new Route($entity_type->getLinkTemplate('canonical'));
    $route
      ->addDefaults([
        '_controller' => "\Drupal\simple_survey\Controller\SimpleSurveyController::page",
        '_title_callback' => '\Drupal\simple_survey\Controller\SimpleSurveyController::pageTitle',
      ])
      ->setRequirement('_entity_access', "{$entity_type_id}.view")
      ->setOption('parameters', [
        $entity_type_id => ['type' => 'entity:' . $entity_type_id],
      ])
      ->setOption('_admin_route', TRUE);

    return $route;
  }

  /**
   * {@inheritDoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);
    $entity_type_id = $entity_type->id();

    if ($questions_route = $this->getQuestionsFormRoute($entity_type)) {
      $collection
        ->add("entity.{$entity_type_id}.questions_form", $questions_route);
    }

    if ($display_route = $this->getDisplayFormRoute($entity_type)) {
      $collection
        ->add("entity.{$entity_type_id}.display_form", $display_route);
    }

    if ($extern_services_route = $this->getExternalServicesFormRoute($entity_type)) {
      $collection
        ->add("entity.{$entity_type_id}.external_services_form", $extern_services_route);
    }

    if ($reset_big_query_route = $this->getResetBigQueryRoute($entity_type)) {
      $collection
        ->add("entity.{$entity_type_id}.resetBigQuery", $reset_big_query_route);
    }

    if ($export_csv_route = $this->getCsvExportRoute($entity_type)) {
      $collection
        ->add("entity.{$entity_type_id}.export_csv", $export_csv_route);
    }

    if ($export_form_route = $this->getExportFormRoute($entity_type)) {
      $collection
        ->add("entity.{$entity_type_id}.export_form", $export_form_route);
    }

    return $collection;
  }

  /**
   * Gets the questions-form route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getQuestionsFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('questions-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('questions-form'));

      $route->setDefaults([
        '_entity_form' => "{$entity_type_id}.questions",
        '_title' => 'Questions',
      ])
        ->setRequirement('_entity_access', "{$entity_type_id}.update")
        ->setOption('parameters', [
          $entity_type_id => [
            'type' => 'entity:' . $entity_type_id,
          ],
        ])
        ->setOption('_admin_route', TRUE);

      return $route;
    }
  }

  /**
   * Gets the display-form route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getDisplayFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('display-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('display-form'));

      $route->setDefaults([
        '_entity_form' => "{$entity_type_id}.display",
        '_title' => 'Display',
      ])
        ->setRequirement('_entity_access', "{$entity_type_id}.update")
        ->setOption('parameters', [
          $entity_type_id => [
            'type' => 'entity:' . $entity_type_id,
          ],
        ])
        ->setOption('_admin_route', TRUE);

      return $route;
    }
  }

  /**
   * Gets the external-services-form route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getExternalServicesFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('external-services-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('external-services-form'));

      $route->setDefaults([
        '_entity_form' => "{$entity_type_id}.external_services",
        '_title' => 'External Services',
      ])
        ->setRequirement('_entity_access', "{$entity_type_id}.update")
        ->setOption('parameters', [
          $entity_type_id => [
            'type' => 'entity:' . $entity_type_id,
          ],
        ])
        ->setOption('_admin_route', TRUE);

      return $route;
    }
  }

  /**
   * Gets the BigQuery reset route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getResetBigQueryRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('reset-bigquery')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('reset-bigquery'));

      $route->setDefaults([
        '_controller' => "\Drupal\simple_survey\Controller\BigQueryController::resetBigQuery",
        '_title' => 'Reset BigQuery Data',
      ])
        ->setRequirement('_entity_access', "{$entity_type_id}.update")
        ->setOption('parameters', [
          $entity_type_id => [
            'type' => 'entity:' . $entity_type_id,
          ],
        ])
        ->setOption('_admin_route', TRUE);

      return $route;
    }
  }

  /**
   * Gets the CSV export route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getCsvExportRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('export-csv')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('export-csv'));

      $route->setDefaults([
        '_entity_form' => "{$entity_type_id}.export_csv",
        '_title' => 'Export Responses (CSV)',
      ])
        ->setRequirement('_entity_access', "{$entity_type_id}.update")
        ->setOption('parameters', [
          $entity_type_id => [
            'type' => 'entity:' . $entity_type_id,
          ],
        ])
        ->setOption('_admin_route', TRUE);

      return $route;
    }
  }

  /**
   * Gets the Export form route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getExportFormRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('export-form')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('export-form'));

      $route->setDefaults([
        '_entity_form' => "{$entity_type_id}.export",
        '_title' => 'Export Responses',
      ])
        ->setRequirement('_entity_access', "{$entity_type_id}.update")
        ->setOption('parameters', [
          $entity_type_id => [
            'type' => 'entity:' . $entity_type_id,
          ],
        ])
        ->setOption('_admin_route', TRUE);

      return $route;
    }
  }

}
