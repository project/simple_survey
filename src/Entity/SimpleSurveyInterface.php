<?php

namespace Drupal\simple_survey\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Survey Response entities.
 *
 * @ingroup simple_survey
 */
interface SimpleSurveyInterface extends ConfigEntityInterface {

  /**
   * Checks if the entered paths match the current path.
   *
   * @param string|null $path
   *   The current aliased path.
   *
   * @return bool
   *   Returns true if a match is found false if not.
   */
  public function checkPath(string $path = NULL);

  /**
   * Checks survey is active.
   *
   * @return bool
   *   Returns true if survey is active false if inactive.
   */
  public function isSurveyActive();

  /**
   * Checks if survey is submitted.
   *
   * @param string $uuid
   *   The current user.
   * @param string $currentPath
   *   The current path.
   *
   * @return bool
   *   The returned response.
   */
  public function checkSubmitted($uuid, $currentPath);

}
