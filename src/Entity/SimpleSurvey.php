<?php

namespace Drupal\simple_survey\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\simple_survey\Entity\SimpleSurveyInterface;

/**
 * Defines the SimpleSurvey entity.
 *
 * @ConfigEntityType(
 *   id = "simple_survey",
 *   label = @Translation("Simple feedback survey"),
 *   label_collection = @Translation("Simple feedback surveys"),
 *   label_singular = @Translation("Simple feedback survey"),
 *   label_plural = @Translation("Simple feedback surveys"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Simple feedback survey",
 *     plural = "@count Simple feedback surveys",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\simple_survey\Entity\SimpleSurveyListBuilder",
 *     "form" = {
 *       "default" = "Drupal\simple_survey\Form\SimpleSurveyForm",
 *       "add" = "Drupal\simple_survey\Form\SimpleSurveyForm",
 *       "edit" = "Drupal\simple_survey\Form\SimpleSurveyForm",
 *       "export_csv" = "Drupal\simple_survey\Form\ConfirmCsvExportForm",
 *       "external_services" = "Drupal\simple_survey\Form\ExternalServicesForm",
 *       "questions" = "Drupal\simple_survey\Form\SurveyQuestionsListForm",
 *       "display" = "Drupal\simple_survey\Form\SurveyDisplayForm",
 *       "delete" = "Drupal\simple_survey\Form\SimpleSurveyDeleteForm",
 *       "export" = "Drupal\simple_survey\Form\ExportForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\simple_survey\Entity\Routing\SimpleSurveyRoutingProvider",
 *     },
 *   },
 *   config_prefix = "simple_survey",
 *   admin_permission = "administer simple survey configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "debug",
 *     "description",
 *     "basePath",
 *     "questionsPath",
 *     "submitPath",
 *     "validationPath",
 *     "enableTranslation",
 *     "anchorTag",
 *     "attachStylesheet",
 *     "displayDelayType",
 *     "delay",
 *     "delayPosition",
 *     "displayOnceOnSite",
 *     "closingDelay",
 *     "bigQueryProjectId",
 *     "bigQueryKeyfile",
 *     "bigQueryDataset",
 *     "bigQueryTable",
 *     "questionWeights",
 *     "useFontAwesome",
 *     "displayFilterByPath",
 *     "startingQuestion",
 *     "enableSurvey",
 *     "numberOfResponsesToStopAt",
 *     "displayCooldown",
 *     "answeredDisplayCooldown",
 *     "surveyActiveFrom",
 *     "surveyActiveUntil",
 *     "ombDisclaimer",
 *     "ombId",
 *     "ombExpiration",
 *     "ombExpirationFormat",
 *     "ombLink",
 *     "surveyWhenLoggedIn"
 *   },
 *   links = {
 *     "canonical" = "/admin/content/simple_survey/{simple_survey}",
 *     "add-form" = "/admin/content/simple_survey/add",
 *     "edit-form" = "/admin/content/simple_survey/{simple_survey}/edit",
 *     "questions-form" = "/admin/content/simple_survey/{simple_survey}/edit/questions",
 *     "display-form" = "/admin/content/simple_survey/{simple_survey}/edit/display",
 *     "reset-bigquery" = "/admin/content/simple_survey/{simple_survey}/reset-bigquery",
 *     "external-services-form" = "/admin/content/simple_survey/{simple_survey}/edit/external-services",
 *     "delete-form" = "/admin/content/simple_survey/{simple_survey}/delete",
 *     "collection" = "/admin/content/simple_survey",
 *     "export-csv" = "/admin/content/simple_survey/{simple_survey}/responses/export/csv",
 *     "export-form" = "/admin/content/simple_survey/{simple_survey}/responses/export",
 *   }
 * )
 */
class SimpleSurvey extends ConfigEntityBase implements SimpleSurveyInterface {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Survey in debug mode.
   *
   * @var bool
   */
  public $debug;

  /**
   * Survey Base Path.
   *
   * @var string
   */
  public $basePath;

  /**
   * Survey Question Path.
   *
   * @var string
   */
  public $questionsPath;

  /**
   * Survey Submission Path.
   *
   * @var string
   */
  public $submitPath;

  /**
   * Survey validation Path.
   *
   * @var string
   */
  public $validationPath;

  /**
   * Enable Translation.
   *
   * @var bool
   */
  public $enableTranslation;

  /**
   * The SimpleSurvey ID.
   *
   * @var string
   */
  public $id;

  /**
   * The SimpleSurvey label.
   *
   * @var string
   */
  public $label;

  /**
   * A brief description of this simple feedback survey.
   *
   * @var string
   */
  public $description;

  /**
   * HTML tag or ID Survey should anchor to.
   *
   * @var string
   */
  public $anchorTag;

  /**
   * Attach module stylesheet.
   *
   * @var bool
   */
  public $attachStylesheet;

  /**
   * How should the survey display.
   *
   * @var string
   */
  public $displayDelayType;

  /**
   * How long before the survey should appear.
   *
   * @var int
   */
  public $delay;

  /**
   * Only display once on the entire site.
   *
   * @var bool
   */
  public $displayOnceOnSite;

  /**
   * Amount of time before survey closes on last screen.
   *
   * @var int
   */
  public $closingDelay;

  /**
   * If using positional delay where should it appear.
   *
   * @var string
   */
  public $delayPosition;

  /**
   * Use font awesome if it is available.
   *
   * @var bool
   */
  public $useFontAwesome;

  /**
   * Project ID in BigQuery.
   *
   * @var string
   */
  public $bigQueryProjectId;

  /**
   * BigQuery dataset.
   *
   * @var string
   */
  public $bigQueryDataset;

  /**
   * BigQuery key file text.
   *
   * @var string
   */
  public $bigQueryKeyfile;

  /**
   * Table used in BigQuery.
   *
   * @var bool
   */
  public $bigQueryTable;

  /**
   * Question weights.
   *
   * @var array
   */
  public $questionWeights;

  /**
   * Display page filtering.
   *
   * @var array
   */
  public $displayFilter;

  /**
   * Display page filtering path.
   *
   * @var array
   */
  public $displayFilterByPath;

  /**
   * The starting Question.
   *
   * @var array
   */
  public $startingQuestion;

  /**
   * Display page filtering.
   *
   * @var int
   */
  public $displayCooldown;

  /**
   * Display page filtering path.
   *
   * @var int
   */
  public $answeredDisplayCooldown;

  /**
   * Enables survey.
   *
   * @var bool
   */
  public $enableSurvey;

  /**
   * Date survey should be active from.
   *
   * @var string
   */
  public $surveyActiveFrom;

  /**
   * Date survey should be active to.
   *
   * @var string
   */
  public $surveyActiveUntil;

  /**
   * If the form should display and Privacy Disclaimer.
   *
   * @var bool
   */
  public $ombDisclaimer;

  /**
   * Privacy document ID if any.
   *
   * @var string
   */
  public $ombId;

  /**
   * Expiration of the document.
   *
   * @var string
   */
  public $ombExpiration;

  /**
   * Expiration date format.
   *
   * @var string
   */
  public $ombExpirationFormat;

  /**
   * Link to privacy document.
   *
   * @var string
   */
  public $ombLink;

  /**
   * Number of responses survey should stop collecting data at.
   *
   * @var int
   */
  public $numberOfResponsesToStopAt;

  /**
   * Show the survey when a user is logged in.
   *
   * @var bool
   */
  public $surveyWhenLoggedIn;

  /**
   * Returns the debug property.
   *
   * @return bool
   *   The debug property value.
   */
  public function debug() {
    return $this->debug;
  }

  /**
   * Returns the anchorTag property.
   *
   * @return string
   *   The anchorTag property value.
   */
  public function anchorTag() {
    return $this->anchorTag;
  }

  /**
   * {@inheritdoc}
   */
  public function description() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getDebug() {
    return $this->debug;
  }

  /**
   * {@inheritDoc}
   */
  public function checkPath(string $path = NULL) {
    /**
     * @var \Drupal\Core\Path\CurrentPathStack $currentPath
     *   Drupal Patch Matcher service.
     */
    $currentPath = \Drupal::service('path.current')->getPath();
    /**
     * @var \Drupal\path_alias\AliasManager $currentPath
     *   Drupal Patch Matcher service.
     */
    $currentAlias = \Drupal::service('path_alias.manager')->getAliasByPath($currentPath);
    /**
     * @var \Drupal\Core\Path\PathMatcher $pathMatcher
     *   Drupal Patch Matcher service.
     */
    $pathMatcher = \Drupal::service('path.matcher');

    return $pathMatcher->matchPath($currentAlias, $this->displayFilterByPath);
  }

  /**
   * {@inheritDoc}
   */
  public function isSurveyActive() {
    return $this->get('enableSurvey') && !$this->reachedMaximumResponses()
      && !$this->reachedSurveyExpiration();
  }

  /**
   * Get the survey that loads on this path.
   *
   * @return null|\Drupal\simple_survey\Entity\SimpleSurveyInterface
   *   Returns either null or the Simple Survey entity.
   */
  public static function getSurvey() {

    $surveys = self::loadMultiple();
    $logged_in = \Drupal::currentUser()->isAuthenticated();

    foreach ($surveys as $survey) {
      if ($survey) {
        if (!$survey->get('enableSurvey')) {
          continue;
        }

        if ($logged_in && !$survey->get('surveyWhenLoggedIn')) {
          continue;
        }

        if (!$survey->hasClosingQuestion() || !$survey->get('startingQuestion')) {
          continue;
        }

        if ($survey->checkPath()) {
          if ($survey->reachedMaximumResponses()) {
            return NULL;
          }

          if ($survey->reachedSurveyExpiration()) {
            return NULL;
          }

          return $survey;
        }
        elseif ($survey->debug()) {
          \Drupal::service('logger.factory')->get('simple_survey')->info(t('Could not load survey on %path with Paths %paths', [
            '%path' => \Drupal::service('path.current')->getPath(),
            '%paths' => $survey->displayFilterByPath,
          ]));
        }
      }
    }

    return NULL;
  }

  /**
   * Checks survey not expired.
   */
  public function reachedSurveyExpiration() {
    $endDate = !empty($this->get('surveyActiveUntil')) ?
      strtotime($this->get('surveyActiveUntil')) + 86400 : NULL;
    $currentDate = \Drupal::time()->getRequestTime();

    return $endDate ? $endDate <= $currentDate : FALSE;
  }

  /**
   * Gets the maximum number of responses.
   */
  public function reachedMaximumResponses() {
    $numberOfResponses = \Drupal::service('entity_type.manager')->getStorage('survey_response')->getQuery()
      ->condition('survey', $this->id())
      ->count()
      ->execute();

    $maxNumberOfResponses = !is_null($this->get('numberOfResponsesToStopAt')) ?
      $this->get('numberOfResponsesToStopAt') : 0;

    return $maxNumberOfResponses > 0 ? $numberOfResponses >= $maxNumberOfResponses : FALSE;
  }

  /**
   * Checks if this survey has a closing question.
   *
   * @return bool
   *   Returns true if the count query returns greater than zero.
   */
  public function hasClosingQuestion() {
    return \Drupal::service('entity_type.manager')->getStorage('survey_question')->getQuery()
      ->condition('survey', $this->id())
      ->condition('question_type', 'closing')
      ->count()
      ->execute() > 0;
  }

  /**
   * Sets the last time the data was submitted to BigQuery.
   */
  public function setBqSubmission() {
    /** @var \Drupal\simple_survey\Service\SurveyState $state */
    $state = \Drupal::service('simple_survey.survey_state');

    $state->setLastExported($this->id());
  }

  /**
   * Gets the last time the data was submitted to BigQuery.
   *
   * @return mixed
   *   The stored last submitted value or NULL.
   */
  public function getBqSubmission() {
    /** @var \Drupal\simple_survey\Service\SurveyState $state */
    $state = \Drupal::service('simple_survey.survey_state');
    return $state->getLastExported($this->id());
  }

  /**
   * Checks Survey Status and displays appropriate messages.
   */
  public function checkStatuses() {

    if ($this->reachedMaximumResponses()) {
      $this->messenger()->addError($this->t('The %label survey has reach the maximum number of responses. <br> No more responses will be collected.', [
        '%label' => $this->label(),
      ]));
    }

    if ($this->reachedSurveyExpiration()) {
      $this->messenger()->addError($this->t('The %label survey has expired. <br> No more responses will be collected.', [
        '%label' => $this->label(),
      ]));
    }
    elseif ($this->get('surveyActiveUntil')) {
      $remainingTime = (strtotime($this->get('surveyActiveUntil')) + 86400) -
        \Drupal::time()->getRequestTime();

      $statusType = MessengerInterface::TYPE_ERROR;

      if ($remainingTime > (86400 * 7)) {
        $statusType = MessengerInterface::TYPE_STATUS;
      }
      elseif ($remainingTime <= (86400 * 7) && $remainingTime >= 86400) {
        $statusType = MessengerInterface::TYPE_WARNING;
      }

      $this->messenger()->addMessage($this->t('The %label survey will expire %expiration', [
        '%label' => $this->label(),
        '%expiration' => $this->get('surveyActiveUntil'),
      ]), $statusType);
    }

  }

  /**
   * {@inheritDoc}
   */
  public function checkSubmitted($uuid, $currentPath) {

    $srids = \Drupal::service('entity_type.manager')->getStorage('survey_response')->getQuery()
      ->condition('cookie_uuid', $uuid)
      ->condition('site_path', $currentPath)
      ->count()
      ->execute();

    return $srids > 0;
  }

}
