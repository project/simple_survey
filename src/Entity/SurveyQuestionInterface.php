<?php

namespace Drupal\simple_survey\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface for defining Survey Question entities.
 *
 * @ingroup simple_survey
 */
interface SurveyQuestionInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets the Survey Response creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Survey Response.
   */
  public function getCreatedTime();

  /**
   * Sets the Survey Response creation timestamp.
   *
   * @param int $timestamp
   *   The Survey Response creation timestamp.
   *
   * @return \Drupal\simple_survey\Entity\SurveyResponseInterface
   *   The called Survey Response entity.
   */
  public function setCreatedTime($timestamp);

}
