<?php

namespace Drupal\simple_survey;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Survey Response entity.
 *
 * @see \Drupal\simple_survey\Entity\SurveyResponse.
 */
class SurveyResponseAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\simple_survey\Entity\SurveyResponseInterface $entity */
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view survey response entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete survey response entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

}
