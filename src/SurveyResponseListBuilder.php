<?php

namespace Drupal\simple_survey;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Survey Response entities.
 *
 * @ingroup simple_survey
 */
class SurveyResponseListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Survey Response ID');
    $header['uuid'] = $this->t('uuid');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\simple_survey\Entity\SurveyResponse */
    $row['id'] = $entity->id();
    $row['uuid'] = $entity->get('uuid');

    return $row + parent::buildRow($entity);
  }

}
