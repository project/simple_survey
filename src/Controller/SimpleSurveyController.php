<?php

namespace Drupal\simple_survey\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\File\FileSystemInterface;
use Drupal\simple_survey\Entity\SimpleSurveyInterface;
use Drupal\simple_survey\Service\SurveyState;

/**
 * Class SimpleSurveyController.
 *
 * @package Drupal\simple_survey\Controller
 */
class SimpleSurveyController extends ControllerBase {

  /**
   * Drupal date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The survey state service.
   *
   * @var \Drupal\simple_survey\Service\SurveyState
   */
  protected $surveyState;

  /**
   * File System service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * Simple Survery Controller constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystemInterface
   *   The Drupal file system service.
   */
  public function __construct(
    SurveyState $surveyState,
    FileSystemInterface $fileSystemInterface) {
    $this->fileSystem = $fileSystemInterface;
    $this->surveyState = $surveyState;

    $this->modulePath = drupal_get_path('module', 'simple_survey');
  }

  /**
   * Dependency Injection.
   *
   * @inheritDoc
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('simple_survey.survey_state'),
      $container->get('file_system')
    );
  }

  /**
   * Allows user to download the temporary generated response survey.
   *
   * @param string $simple_survey
   *   The survey machine name.
   * @param string $filename
   *   The survey filename.
   *
   * @return mixed
   *   Will return either a file or redirect to the requesting survey.
   */
  public function getTemporaryFile($simple_survey, $filename) {

    $filepath = $this->fileSystem->realpath("temporary://")
      . '/' . $filename;

    if (file_exists($filepath)) {
      $file = new \SplFileObject($filepath, 'r');

      $headerResponse = new Response();
      $headerResponse->headers->set('Content-Type', 'test/csv; charset=utf-8');
      $headerResponse->headers->set('Content-Disposition', 'attachment; filename="'
      . $filename);
      $headerResponse->headers->set('Content-Length', $file->getSize());
      $headerResponse->sendHeaders();

      $file->fpassthru();

      return $headerResponse;
    }
    else {
      $this->messenger()->addWarning($this->t(':filename no longer exist. Please generate a new download.', [
        ':filename' => $filename,
      ]));

      return $this->redirect('entity.simple_survey.edit_form', [
        'simple_survey' => $simple_survey,
      ]);
    }
  }

  /**
   * Displays information about a survey.
   *
   * @param \Drupal\simple_survey\Entity\SimpleSurveyInterface $simple_survey
   *   The survey to display.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function page(SimpleSurveyInterface $simple_survey) {
    $lastExported = $this->surveyState->getLastExported($simple_survey->id());

    // Build the search index information.
    $render = [
      'view' => [
        '#theme' => 'simple_survey',
        '#index' => $simple_survey,
        '#questionsPath' => $simple_survey->get('questionsPath'),
        '#submitPath' => $simple_survey->get('submitPath'),
        '#lastExported' => $lastExported,
      ],
    ];

    return $render;
  }

  /**
   * Returns the page title for an survey's "View" tab.
   *
   * @param \Drupal\simple_survey\Entity\SimpleSurveyInterface $simple_survey
   *   The survey that is displayed.
   *
   * @return string
   *   The page title.
   */
  public function pageTitle(SimpleSurveyInterface $simple_survey) {
    return new FormattableMarkup('@title', ['@title' => $simple_survey->label()]);
  }

}