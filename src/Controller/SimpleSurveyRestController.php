<?php

namespace Drupal\simple_survey\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\simple_survey\Entity\SurveyResponse;
use Drupal\simple_survey\Entity\SurveyAnswer;
use Drupal\Core\Entity\EntityStorageException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Snipe\BanBuilder\CensorWords;
use Drupal\simple_survey\Service\BigQueryConnect;
use Drupal\simple_survey\Service\ResponseExportService;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\simple_survey\Entity\SimpleSurvey;
use Drupal\simple_survey\Entity\SimpleSurveyInterface;
use Drupal\simple_survey\Entity\SurveyQuestion;

/**
 * Class FeedbackjsController.
 *
 * @package Drupal\simple_survey\Controller
 */
class SimpleSurveyRestController extends ControllerBase {

  /**
   * The survey.
   *
   * @var \Drupal\simple_survey\Entity\SimpleSurveyInterface
   */
  private $survey = '';

  /**
   * The modules machine name.
   *
   * @var string
   */
  private $moduleName = 'simple_survey';

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Component\Uuid\UuidInterface definition.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidService;

  /**
   * Drupal\Core\Logger\LoggerChannelFactory definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Drupal date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * Response export service.
   *
   * @var \Drupal\simple_survey\Service\BigQueryConnect
   */
  protected $bigQuery;

  /**
   * Response export service.
   *
   * @var \Drupal\simple_survey\ResponseExportService
   */
  protected $responseExport;

  /**
   * The Drupal Time Service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The Entity Type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Simple Survery Controller constructor.
   *
   * @todo Look into creating a service to handle DI. Doing a janky call in simple_survey.module
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Drupal messenger service.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The Drupal UUID service.
   * @param Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   The Drupal date formatter service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   * @param \Drupal\simple_survey\Service\BigQueryConnect $big_query
   *   The BigQuery Connection service.
   * @param \Drupal\simple_survey\ResponseExportService $response_export_service
   *   The response export service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Drupal time service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager service.
   */
  public function __construct(
    MessengerInterface $messenger,
    UuidInterface $uuid,
    DateFormatter $dateFormatter,
    LoggerChannelFactoryInterface $logger_factory,
    BigQueryConnect $big_query,
    ResponseExportService $response_export_service,
    TimeInterface $time,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->messenger = $messenger;
    $this->uuidService = $uuid;
    $this->dateFormatter = $dateFormatter;
    $this->logger = $logger_factory->get($this->moduleName);
    $this->bigQuery = $big_query;
    $this->responseExport = $response_export_service;
    $this->time = $time;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Dependency Injection.
   *
   * @inheritDoc
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('uuid'),
      $container->get('date.formatter'),
      $container->get('logger.factory'),
      $container->get('simple_survey.big_query'),
      $container->get('simple_survey.response_export'),
      $container->get('datetime.time'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Generates a Cookie for tracking and CSRF.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   Returns a http response.
   */
  private function generateCookie(Request $request, string $cookieName) {
    /**
     * @var int $cooldown
     *   The multiplier for $expireTime. Converts $expireTime to days.
     */
    $cooldown = $this->survey->get('answeredDisplayCooldown') ?: 1;
    /**
     * @var int $expireTime
     *   The amount of time before a survey displays after being answered.
     *   Defaults to one day from request time.
     */
    $expireTime = $this->time->getRequestTime() + (60 * 60 * 24 * $cooldown);

    $headerResponse = new Response();
    $cookie = new Cookie(
      $cookieName,
      $this->uuidService->generate(),
      $expireTime,
      '/',
      $request->getHost(),
      TRUE,
      FALSE,
      FALSE,
      'strict'
    );

    if ($cookie) {
      $headerResponse->headers->setCookie($cookie);
      $headerResponse->setStatusCode(Response::HTTP_OK);
      $headerResponse->setContent('Credential Set.');

      return $headerResponse;
    }
    else {
      $headerResponse->setStatusCode(Response::HTTP_NO_CONTENT);
      $headerResponse->setContent('Invalid Request Received');

      return $headerResponse;
    }
  }

  /**
   * Checks if request to load a survey is valid and should be loaded displayed.
   *
   * @param Symfony\Component\HttpFoundation\Request $request
   *   The Drupal request.
   * @param Drupal\simple_survey\Entity\SimpleSurveyInterface $simple_survey
   *   The Simple survey config entity.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   The returned response.
   */
  public function validate(Request $request, SimpleSurveyInterface $simple_survey) {
    $headerResponse = new Response();
    $this->survey = $simple_survey;

    $cookieName = $this->moduleName . '_' . $this->survey->id();

    if (!empty($request->cookies->get($cookieName . '_pause'))) {
      $headerResponse->setStatusCode(Response::HTTP_NO_CONTENT);
      $headerResponse->setContent('Invalid Request Received');

      return $headerResponse;
    }
    elseif (empty($request->cookies->get($cookieName))) {
      return $this->generateCookie($request, $cookieName);
    }
    else {
      $srids = $this->entityTypeManager->getStorage('survey_response')->getQuery()
        ->condition('cookie_uuid', $request->cookies->get($cookieName))
        ->condition('site_path', Html::escape($request->get('site_path')))
        ->count()
        ->execute();

      if ($srids > 0) {
        $headerResponse->setStatusCode(Response::HTTP_NO_CONTENT);
        $headerResponse->setContent('Invalid Request Received');

        return $headerResponse;
      }
      else {
        $headerResponse->setStatusCode(Response::HTTP_OK);
        $headerResponse->setContent('Valid Request');

        return $headerResponse;
      }
    }
  }

  /**
   * Displays the questions as JSON.
   *
   * @param Drupal\simple_survey\Entity\SimpleSurveyInterface $simple_survey
   *   The Simple survey config entity.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Returns a JSON Response.
   */
  public function displayQuestions(SimpleSurveyInterface $simple_survey) {
    $output = [];

    $query = $this->entityTypeManager()->getStorage('survey_question')->getQuery();

    $sqids = $query->condition('survey', $simple_survey->id())
      ->execute();

    $questions = SurveyQuestion::loadMultiple($sqids);

    foreach ($questions as $question) {
      $genericId = 'sq' . $question->id();
      $output[$genericId] = [
        'startingQuestion' => $simple_survey->get('startingQuestion') == $question->id(),
        'inputType' => $question->get('input_type')->value,
        'displayAs' => $question->get('display_as')->value,
        'questionType' => $question->get('question_type')->value,
        'id' => $genericId,
        'disclaimerText' => [
          'en' => $question->get('question_disclaimer')->value,
        ],
        'text' => [
          'en' => $question->get('question')->value,
        ],
        'placeholder' => [
          'en' => $question->get('placeholder_text')->value ?: '',
        ],
      ];

      foreach ($question->get('next_question') as $reference) {
        $nextQuestion = 'sq' . $reference->entity->id();
        $output[$genericId]['dependencies']['next_question'] = $nextQuestion;
      }

      foreach ($question->get('branching') as $reference) {
        $selectedValues = explode('|', $reference->question_option);

        $output[$genericId]['dependencies'][$selectedValues[0]]['destination'] = 'sq' . $reference->entity->id();
        $output[$genericId]['dependencies'][$selectedValues[0]]['source'] = isset($selectedValues[1]) ? $selectedValues[1] : NULL;
      }

      foreach ($question->get('options')->getValue() as $key => $value) {
        switch ($output[$genericId]['inputType']) {
          case 'select':
            $output[$genericId]['options']['en'][] = $value['value'];
            break;

          case 'radio':
            $output[$genericId]['options'][] = [
              'id' => $genericId . '_' . $key,
              'name' => $genericId,
              'text' => [
                'en' => $value['value'],
              ],
              'value' => $value['value'],
            ];
            break;

          case 'checkbox':
            $output[$genericId]['options'][] = [
              'id' => $genericId . '_' . $key,
              'name' => $genericId . '[]',
              'text' => [
                'en' => $value['value'],
              ],
              'value' => $value['value'],
            ];
            break;
        }
      }
    }

    $response = new CacheableJsonResponse($output, 200);

    return $response;
  }

  /**
   * Submits the responses.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The drupal request.
   * @param Drupal\simple_survey\Entity\SimpleSurveyInterface $simple_survey
   *   The Simple survey config entity.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Returns a HTTP Response.
   */
  public function submitForm(Request $request, SimpleSurveyInterface $simple_survey) {
    $headerResponse = new Response();
    $censor = new CensorWords();
    $this->survey = $simple_survey;

    $cookie_uuid = Xss::filter(trim($request->get('uuid', $this->uuidService->generate())));

    if (empty($cookie_uuid) || $cookie_uuid === 'null') {
      $cookie_uuid = $this->uuidService->generate();
    }

    $sitePath = Xss::filter($request->get('site_path'));

    $cookieValidation = $simple_survey->checkSubmitted($cookie_uuid, $sitePath);

    if ($cookieValidation) {
      $headerResponse->setStatusCode(Response::HTTP_OK);
      $headerResponse->setContent($cookie_uuid);

      return $headerResponse;
    }

    $surveyResponse = SurveyResponse::create();
    $surveyResponse->set('cookie_uuid', $cookie_uuid);
    $surveyResponse->set('survey', $simple_survey);
    $surveyResponse->set('created', $this->time->getRequestTime());

    $surveyResponse->set('site_path', $sitePath);
    $surveyResponse->set('langcode', Html::escape($request->get('language')));

    try {
      $surveyResponse->save();
      $questions = SurveyQuestion::loadMultiple();

      foreach ($questions as $question) {
        $profanityFilteredAnswer = $censor->censorString(Xss::filter($request->get('sq' . $question->id())));
        $questionAnswer = str_replace(['\r', '\n'], ' ', $profanityFilteredAnswer['clean']);

        if ($questionAnswer) {
          $surveyAnswer = SurveyAnswer::create();

          $surveyAnswer->set('parent_uuid', $surveyResponse->uuid());
          $surveyAnswer->set('answer', $questionAnswer ?: '');
          $surveyAnswer->set('question_id', $question->id());

          try {
            $surveyAnswer->save();
          }
          catch (EntityStorageException $e) {
            $this->logger->alert($e);

            $headerResponse->setStatusCode(Response::HTTP_NO_CONTENT);
            $headerResponse->setContent($cookie_uuid);

            return $headerResponse;
          }
        }
      }

      $headerResponse->setStatusCode(Response::HTTP_OK);
      $headerResponse->setContent($cookie_uuid);

      return $headerResponse;
    }
    catch (EntityStorageException $e) {
      $this->logger->alert($e);

      $headerResponse->setStatusCode(Response::HTTP_NO_CONTENT);
      $headerResponse->setContent('Invalid Request Received');

      return $headerResponse;
    }
  }

}
