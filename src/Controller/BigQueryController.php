<?php

namespace Drupal\simple_survey\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Url;
use Drupal\simple_survey\Entity\SimpleSurveyInterface;

/**
 * Class SimpleSurveyController.
 *
 * @package Drupal\simple_survey\Controller
 */
class BigQueryController extends ControllerBase {
  use MessengerTrait;

  /**
   * The modules machine name.
   *
   * @var string
   */
  private $moduleName = 'simple_survey';

  /**
   * File System service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Simple Survery Controller constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The Drupal file system service.
   */
  public function __construct(
    Connection $connection) {
    $this->database = $connection;
    $this->modulePath = drupal_get_path('module', $this->moduleName);
  }

  /**
   * Dependency Injection.
   *
   * @inheritDoc
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * Completion callback for PrintBookForm batch.
   *
   * @param bool $success
   *   Boolean value that specifies batch success.
   * @param array $results
   *   Array that contains results from batch context.
   * @param array $operations
   *   Array that contains batch operations.
   */
  public static function finishedProcessingBq(bool $success, array $results, array $operations) {
    $status = Messenger::TYPE_STATUS;

    if ($success) {
      $message = t('Data sent to BigQuery for :survey.', [
        ':survey' => $results['survey'],
      ]);

      /** @var \Drupal\simple_survey\Service\SurveyState $state */
      $state = \Drupal::service('simple_survey.survey_state');

      $state->setLastExported($results['survey']);

      \Drupal::logger('simple_survey')->info($message);
    }
    else {
      $message = t('Could not send data to BigQuery for :survey.', [
        ':survey' => $results['survey'],
      ]);

      $status = Messenger::TYPE_ERROR;
      \Drupal::logger('simple_survey')->error($message);
    }

    \Drupal::messenger()->addMessage($message, $status);
  }

  /**
   * Manual submission to BigQuery.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Returns response object.
   */
  public function resetBigQuery(SimpleSurveyInterface $simple_survey) {
    $survey = $simple_survey->id();

    $this->database->update('survey_response')
      ->fields([
        'sent_to_bq' => 0,
      ])
      ->condition('survey', $survey)
      ->execute();

    $batch = [
      'title' => $this->t('Send Data to BigQuery for %survey', [
        '%survey' => $simple_survey->label(),
      ]),
      'operations' => [
        [
          '\Drupal\simple_survey\Service\ResponseExportService::sendAllResponses',
          [
            $survey,
          ],
        ],
      ],
      'finished' => '\Drupal\simple_survey\Controller\BigQueryController::finishedProcessingBq',
      'init_message' => $this->t('Resending data to BigQuery for @source', [
        '@source' => $simple_survey->label(),
      ]),
      'progress_message' => $this->t('Time left: @estimate | Time elapsed: @elapsed', [
        '@source' => $simple_survey->label(),
      ]),
      'error_message' => $this->t('The export progress has encountered an error.'),
    ];

    batch_set($batch);

    return batch_process(Url::fromRoute('entity.simple_survey.edit_form', [
      'simple_survey' => $survey,
    ]));
  }

}
