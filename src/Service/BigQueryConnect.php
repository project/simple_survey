<?php

namespace Drupal\simple_survey\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatter;
use Google\Cloud\BigQuery\BigQueryClient;
use Google\Cloud\Core\ExponentialBackoff;
use Drupal\simple_survey\Entity\SimpleSurvey;
use Drupal\simple_survey\Entity\SurveyQuestion;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Drupal BigQuery Service.
 */
class BigQueryConnect {

  /**
   * The module name.
   *
   * @var string
   */
  private $moduleName = 'simple_survey';

  /**
   * Is BigQuery Configured.
   *
   * @var bool
   */
  private $isConfigured = FALSE;

  /**
   * The big query client object.
   *
   * @var \Google\Cloud\BigQuery\BigQueryClient
   */
  private $bigQueryClient;

  /**
   * Drupal logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The current survey.
   *
   * @var \Drupal\simple_survey\Entity\SimpleSurvey
   */
  public $survey;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Service config and DI.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Drupal messenger service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Drupal config factory service.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   Date formatter service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   */
  public function __construct(
    MessengerInterface $messenger,
    ConfigFactoryInterface $configFactory,
    DateFormatter $dateFormatter,
    LoggerChannelFactoryInterface $logger_factory
  ) {
    $this->messenger = $messenger;
    $this->configFactory = $configFactory;
    $this->dateFormatter = $dateFormatter;
    $this->logger = $logger_factory->get($this->moduleName);
  }

  /**
   * Sets the active survey.
   *
   * @param \Drupal\simple_survey\Entity\SimpleSurvey $survey
   *   The active survey.
   */
  public function setSurvey(SimpleSurvey $survey) {
    $this->survey = $survey;
    $this->isConfigured = FALSE;

    if ($this->survey->get('bigQueryKeyfile')) {
      $this->bigQueryClient = new BigQueryClient([
        'projectId' => $this->survey->get('bigQueryProjectId'),
        'keyFile' => json_decode($this->survey->get('bigQueryKeyfile'), TRUE),
      ]);

      $dataset = $this->survey->get('bigQueryDataset');
      $table = $this->survey->get('bigQueryTable');

      $this->isConfigured = (!empty($dataset) && !empty($table));
    }

  }

  /**
   * Checks Simple Survey configured to use BigQuery.
   *
   * @return bool
   *   Returns true if configured, false if not.
   */
  public function checkConfigured() {
    return $this->isConfigured;
  }

  /**
   * Send table data to BigQuery and overwrites existing data.
   *
   * @param string $dataset
   *   The name of the BigQuery dataset.
   * @param string $table
   *   The name of the BigQuery table.
   * @param mixed $data
   *   The converted data.
   *
   * @return bool
   *   Returns true if updated, false if failed.
   */
  public function sendJsonData(string $dataset, string $table, $data, $truncate = FALSE) {
    $dataset = $this->bigQueryClient->dataset($dataset);
    $table = $dataset->table($table);

    // Loads the CSV output.
    $loadConfig = $table->load($data);
    $loadConfig->schema($this->buildJsonSchema());
    // Sets the format to CSV.
    $loadConfig->sourceFormat('NEWLINE_DELIMITED_JSON');

    if ($truncate) {
      $loadConfig->writeDisposition('WRITE_TRUNCATE');
    }

    $job = $table->runJob($loadConfig);

    // Poll the job until it is complete.
    $backoff = new ExponentialBackoff(10);
    $backoff->execute(function () use ($job) {
      $job->reload();
    });

    // Check if the job has errors.
    if (isset($job->info()['status']['errorResult'])) {
      $error = $job->info()['status']['errorResult']['message'];
      $this->logger->alert('Error running job: ' . $error . $data);

      return FALSE;
    }
    else {
      return TRUE;
    }
  }

  /**
   * Send table data to BigQuery and overwrites existing data.
   *
   * @param string $dataset
   *   The name of the BigQuery dataset.
   * @param string $table
   *   The name of the BigQuery table.
   * @param mixed $csv
   *   The converted data.
   *
   * @return bool
   *   Returns true if updated, false if failed.
   */
  public function updateTable(string $dataset, string $table, $csv) {
    $dataset = $this->bigQueryClient->dataset($dataset);
    $table = $dataset->table($table);

    // Loads the CSV output.
    $loadConfig = $table->load($csv);
    // Sets the format to CSV.
    $loadConfig->sourceFormat('CSV');
    // Allows BigQuery to autodetect the header and create columns.
    $loadConfig->autodetect(TRUE);
    // Forces the BigQuery table to be overwritten.
    $loadConfig->writeDisposition('WRITE_TRUNCATE');

    $job = $table->runJob($loadConfig);

    // Poll the job until it is complete.
    $backoff = new ExponentialBackoff(10);
    $backoff->execute(function () use ($job) {
      $job->reload();
    });

    // Check if the job has errors.
    if (isset($job->info()['status']['errorResult'])) {
      $error = $job->info()['status']['errorResult']['message'];
      $this->logger->alert('Error running job: ' . $error);

      return FALSE;
    }
    else {
      return TRUE;
    }
  }

  /**
   * Builds a BigQuery JSON schema file.
   *
   * @return array
   *   The survey schema array.
   */
  public function buildJsonSchema() {
    $schema = [
      'fields' => [
        [
          'description' => 'The Drupal response ID',
          'name' => 'drupal_id',
          'type' => 'INT64',
        ],
        [
          'description' => 'User unique identifier',
          'name' => 'cookie_uuid',
          'type' => 'STRING',
        ],
        [
          'description' => 'The page the survey was submitted on',
          'name' => 'site_path',
          'type' => 'STRING',
        ],
        [
          'description' => 'The language of the submission',
          'name' => 'langcode',
          'type' => 'STRING',
        ],
      ],
    ];

    $questionSchema = SurveyQuestion::buildSurveyTableSchema($this->survey->id());

    foreach ($questionSchema as $qs) {
      $schema['fields'][] = $qs;
    }

    $schema['fields'][] = [
      'description' => 'The date submitted',
      'name' => 'created',
      'type' => 'DATETIME',
    ];

    return $schema;
  }

  /**
   * Allows single inserts to BigQuery.
   *
   * @param string $dataset
   *   The name of the BigQuery dataset.
   * @param string $table
   *   The name of the BigQuery table.
   * @param array $response
   *   The response being submitted.
   * @param array $headerArray
   *   The header array.
   *
   * @todo Investigate enabling. Not being used because unsure of traffic
   */
  public function insertRow(string $dataset, string $table, array $response, array $headerArray) {
    $dataset = $this->bigQueryClient->dataset($dataset);
    $table = $dataset->table($table);

    foreach ($headerArray as $key => $label) {
      $output[$label] = isset($response[$key]) ? $response[$key] : '';
    }

    $insertResponse = $table->insertRow($output);

    if ($insertResponse->isSuccessful()) {
      print('Data streamed into BigQuery successfully' . PHP_EOL);
    }
    else {
      foreach ($insertResponse->failedRows() as $row) {
        foreach ($row['errors'] as $error) {
          printf('%s: %s' . PHP_EOL, $error['reason'], $error['message']);
        }
      }
    }
  }

}
