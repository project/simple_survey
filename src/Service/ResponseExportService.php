<?php

namespace Drupal\simple_survey\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use SplTempFileObject;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Url;
use Drupal\simple_survey\Entity\SimpleSurvey;
use Drupal\simple_survey\Entity\SurveyAnswer;
use Drupal\simple_survey\Entity\SurveyQuestion;
use Drupal\simple_survey\Entity\SurveyResponse;
use SplFileObject;

/**
 * Survey Response convert and export service.
 *
 * @todo Add multivalue question value validation.
 */
class ResponseExportService implements ContainerInjectionInterface {

  /**
   * The module name.
   *
   * @var string
   */
  private $moduleName = 'simple_survey';

  /**
   * Drupal config service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Drupal logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Drupal messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The Drupal Time Service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The Entity Type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current survey.
   *
   * @var \Drupal\simple_survey\Entity\SimpleSurvey
   */
  public $survey;

  /**
   * PHP File Object.
   *
   * @var \SplTempFileObject
   */
  public $fileObject;

  /**
   * The export file name.
   *
   * @var string
   */
  public $filename;

  /**
   * Header keys.
   *
   * @var array
   */
  public $headerColumnKeys = [
    'drupal_id',
    'cookie_uuid',
    'site_path',
    'langcode',
  ];

  /**
   * Service config and DI.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Drupal Messenger service.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   Date formatter service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   Drupal logger service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Drupal config factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Time service.
   */
  public function __construct(
    MessengerInterface $messenger,
    DateFormatter $dateFormatter,
    LoggerChannelFactoryInterface $loggerFactory,
    ConfigFactoryInterface $configFactory,
    EntityTypeManagerInterface $entity_type_manager,
    TimeInterface $time
  ) {
    $this->messenger = $messenger;
    $this->configFactory = $configFactory;
    $this->dateFormatter = $dateFormatter;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $loggerFactory->get($this->moduleName);
    $this->time = $time;

    $this->simpleSurveyConfig = $this->configFactory->getEditable($this->moduleName . '.settings');
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('date.formatter'),
      $container->get('logger.factory'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('datetime.time')
    );
  }

  /**
   * Builds the csv header row and sets column keys.
   *
   * @return array
   *   Returns an array of header data.
   */
  public static function createHeaderRow(string $survey) {
    $header = [
      'columns' => [],
      'keys' => [
        'drupal_id',
        'cookie_uuid',
        'site_path',
        'langcode',
      ],
      'validation' => [],
    ];

    $header['columns'] = [
      preg_replace('/[^A-Za-z0-9_]/', '', strtolower(str_replace(' ', '_', 'Drupal ID'))),
      preg_replace('/[^A-Za-z0-9_]/', '', strtolower(str_replace(' ', '_', 'User'))),
      preg_replace('/[^A-Za-z0-9_]/', '', strtolower(str_replace(' ', '_', 'Page Submitted On'))),
      preg_replace('/[^A-Za-z0-9_]/', '', strtolower(str_replace(' ', '_', 'Language'))),
    ];

    $questions = \Drupal::database()->select('survey_question', 'sq')
      ->fields('sq')
      ->condition('sq.survey', $survey)
      ->execute()->fetchAll();

    foreach ($questions as $question) {
      if ($question->question_type == 'normal') {
        $sanitizeQuestion = strip_tags($question->question__value);
        $sanitizeQuestion = str_replace('&nbsp;', ' ', $sanitizeQuestion);
        $sanitizeQuestion = str_replace(["\r", "\n"], '', $sanitizeQuestion);
        $header['columns'][] = preg_replace('/[^A-Za-z0-9_]/', '', strtolower(str_replace(' ', '_', $sanitizeQuestion)));
        $header['keys'][] = $question->id;

        $header['validation'][$question->id] = [];
      }
    }

    $header['columns'][] = preg_replace('/[^A-Za-z0-9_]/', '', strtolower(str_replace(' ', '_', 'Submission was created')));
    $header['keys'][] = 'created';

    return $header;
  }

  /**
   * Gets number of responses for current survey.
   *
   * @return int
   *   The number of responses.
   */
  public static function getResponseCount($survey) {
    $query = \Drupal::database()->select('survey_response', 'sr')
      ->fields('sr');

    if ($survey) {
      $query->condition('survey', $survey);
    }

    return $query
      ->countQuery()
      ->execute()->fetchField();
  }

  /**
   * Builds a response data rray.
   */
  public static function createCsvRow($keys, $validation, $response) {
    $outputResponse = [];

    foreach ($keys as $key) {
      $outputResponse[$key] = NULL;
    }

    $outputResponse['drupal_id'] = $response->id;
    $outputResponse['cookie_uuid'] = $response->cookie_uuid;
    $outputResponse['site_path'] = $response->site_path;
    $outputResponse['langcode'] = $response->langcode;
    $outputResponse['created'] = \Drupal::service('date.formatter')
      ->format($response->created, 'custom', 'Y-m-d H:i:s');

    $survey_answers = SurveyAnswer::loadMultipleBatch($response->uuid);

    foreach ($survey_answers as $survey_answer) {
      $answer = $survey_answer->answer;
      $question_id = $survey_answer->question_id;

      $outputResponse[$question_id] = str_replace(['\r', '\n'], ' ', $answer);
    }

    return $outputResponse;
  }

  /**
   * Get the file object size.
   *
   * @return int
   *   The file size.
   */
  public function getFileSize() {
    return $this->fileObject->getSize();
  }

  /**
   * Batch process method for survey responses.
   *
   * @param string $survey
   *   The survey machine name.
   * @param array|object $context
   *   The batch process context.
   */
  public static function sendAllResponses(string $survey, &$context) {
    $batch_size = 500;
    $truncate = FALSE;

    if (empty($context['sandbox'])) {
      $context['sandbox']['max'] = SurveyResponse::responseCountBatch($survey);
      $context['sandbox']['progress'] = 0;
      $context['results']['survey'] = $survey;
      $truncate = TRUE;
    }

    $max = $context['sandbox']['progress'] + $batch_size;

    if ($max > $context['sandbox']['max']) {
      $max = $context['sandbox']['max'];
    }

    $responses = SurveyResponse::loadMultipleBatch($survey, $context['sandbox']['progress'], $batch_size);

    self::buildNdjsonExportBatch($responses, $survey, $truncate);

    $context['message'] = t('Sent rows @starting through @max of @total.', [
      '@starting' => $context['sandbox']['progress'],
      '@max' => $max,
      '@total' => $context['sandbox']['max'],
    ]);

    $context['sandbox']['progress'] += $batch_size;

    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Batch process method for survey responses.
   *
   * @param array $options
   *   The batch options.
   * @param array|object $context
   *   The batch process context.
   */
  public static function sendResponses(array $options, &$context) {
    $batch_size = 200;
    $truncate = FALSE;
    $db = \Drupal::database();
    $survey = $options['survey'];

    if (empty($context['sandbox'])) {
      $context['sandbox']['max'] = $db->select('survey_response', 'sr')
        ->condition('sr.survey', $survey)
        ->condition('sr.sent_to_bq', $options['sent_to_bq'])
        ->condition('sr.created', strtotime($options['start_date']), '>=')
        ->condition('sr.created', strtotime($options['end_date']), '<=')
        ->countQuery()->execute()->fetchField();
      $context['sandbox']['progress'] = 0;
      $context['results']['survey'] = $survey;
    }

    $max = $context['sandbox']['progress'] + $batch_size;

    if ($max > $context['sandbox']['max']) {
      $max = $context['sandbox']['max'];
    }

    $responses = $db->select('survey_response', 'sr')
      ->fields('sr')
      ->condition('sr.survey', $survey)
      ->condition('sr.sent_to_bq', $options['sent_to_bq'])
      ->condition('sr.created', strtotime($options['start_date']), '>=')
      ->condition('sr.created', strtotime($options['end_date']), '<=')
      ->orderBy('sr.created', 'DESC')
      ->range($context['sandbox']['progress'], $batch_size)
      ->execute()->fetchAll();

    self::buildNdjsonExportBatch($responses, $survey, $truncate);

    $context['message'] = t('Sent rows @starting through @max of @total.', [
      '@starting' => $context['sandbox']['progress'],
      '@max' => $max,
      '@total' => $context['sandbox']['max'],
    ]);

    $context['sandbox']['progress'] += $batch_size;

    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Batch process method for survey responses.
   *
   * @param string $filename
   *   The filename.
   * @param string $survey
   *   The survey machine name.
   * @param array|object $context
   *   The batch process context.
   */
  public static function processData(string $filename, string $survey, &$context) {
    $batch_size = 200;
    $filepath = \Drupal::service('file_system')->realpath("temporary://")
      . '/' . $filename;

    if (empty($context['sandbox'])) {
      $file = new SplFileObject($filepath, 'w');
      $context['sandbox']['headers'] = self::createHeaderRow($survey);
      $context['sandbox']['max'] = self::getResponseCount($survey);
      $context['sandbox']['progress'] = 0;
      $context['results']['filepath'] = $filepath;
      $context['results']['filename'] = $filename;
      $context['results']['survey'] = $survey;
      $file->fputcsv($context['sandbox']['headers']['columns']);
    }
    else {
      $file = new SplFileObject($filepath, 'a');
    }

    $max = $context['sandbox']['progress'] + $batch_size;

    if ($max > $context['sandbox']['max']) {
      $max = $context['sandbox']['max'];
    }

    $responses = SurveyResponse::loadMultipleBatch($survey, $context['sandbox']['progress'], $batch_size);

    $context['message'] = t('Adding rows @starting through @max of @total to @filename.<br>
    Current Size: @fileSize.', [
      '@starting' => $context['sandbox']['progress'],
      '@max' => $max,
      '@total' => $context['sandbox']['max'],
      '@filename' => $filename,
      '@fileSize' => format_size($file->getSize()),
    ]);

    foreach ($responses as $response) {
      $outputResponse = self::createCsvRow(
        $context['sandbox']['headers']['keys'],
        $context['sandbox']['headers']['validation'],
        $response);

      $file->fputcsv($outputResponse);
      $context['sandbox']['progress']++;
    }

    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Batch process method for survey responses.
   *
   * @param array $options
   *   The options array.
   * @param array|object $context
   *   The batch process context.
   */
  public static function processOptionsData(array $options, &$context) {
    $batch_size = 100;
    $db = \Drupal::database();
    $filename = $options['filename'];
    $survey = $options['survey'];
    $filepath = \Drupal::service('file_system')->realpath("temporary://")
      . '/' . $filename;

    if (empty($context['sandbox'])) {
      $file = new SplFileObject($filepath, 'w');
      $context['sandbox']['headers'] = self::createHeaderRow($survey);
      $context['sandbox']['max'] = $db->select('survey_response', 'sr')
        ->condition('sr.survey', $survey)
        ->condition('sr.created', strtotime($options['start_date']), '>=')
        ->condition('sr.created', strtotime($options['end_date']), '<=')
        ->countQuery()->execute()->fetchField();
      $context['sandbox']['progress'] = 0;
      $context['results']['filepath'] = $filepath;
      $context['results']['filename'] = $filename;
      $context['results']['survey'] = $survey;
      $file->fputcsv($context['sandbox']['headers']['columns']);
    }
    else {
      $file = new SplFileObject($filepath, 'a');
    }

    $max = $context['sandbox']['progress'] + $batch_size;

    if ($max > $context['sandbox']['max']) {
      $max = $context['sandbox']['max'];
    }

    $starting = $context['sandbox']['progress'];

    $responses = $db->select('survey_response', 'sr')
      ->fields('sr')
      ->condition('sr.survey', $survey)
      ->condition('sr.created', strtotime($options['start_date']), '>=')
      ->condition('sr.created', strtotime($options['end_date']), '<=')
      ->orderBy('sr.created', 'DESC')
      ->range($starting, $batch_size)
      ->execute()->fetchAll();

    foreach ($responses as $response) {
      $outputResponse = self::createCsvRow(
        $context['sandbox']['headers']['keys'],
        $context['sandbox']['headers']['validation'],
        $response);

      $file->fputcsv($outputResponse);
      $context['sandbox']['progress']++;
    }

    $context['message'] = t('Added rows @starting through @max of @total to @filename.<br>
    Current Size: @fileSize.', [
      '@starting' => $starting,
      '@max' => $max,
      '@total' => $context['sandbox']['max'],
      '@filename' => $filename,
      '@fileSize' => format_size($file->getSize()),
    ]);

    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * CSV file creation atch process finished method.
   *
   * @param bool $success
   *   Boolean value that specifies batch success.
   * @param array $results
   *   Array that contains results from batch context.
   * @param array $operations
   *   Array that contains batch operations.
   */
  public static function finishedFileProcessing(bool $success, array $results, array $operations) {
    $status = Messenger::TYPE_STATUS;

    if ($success) {
      $message = t('CSV File, :filename, created. This file will be available for 6 hours.
      <br>Download: <a href=":link" title=":filename">:filename</a>', [
        ':link' => Url::fromRoute('simple_survey.download_responses', [
          'simple_survey' => $results['survey'],
          'filename' => $results['filename'],
        ])->toString(),
        ':filename' => $results['filename'],
      ]);

      \Drupal::logger('simple_survey')->info($message);
    }
    else {
      $message = t('Failed to build CSV File, :filename.', [
        ':filename' => $results['filename'],
      ]);

      $status = Messenger::TYPE_ERROR;
      \Drupal::logger('simple_survey')->error($message);
    }

    \Drupal::messenger()->addMessage($message, $status);
  }

  /**
   * Builds an NDJSON string out from response IDs.
   *
   * @param array $responses
   *   Array of responses ids.
   *
   * @return mixed
   *   Null|NDJSON string.
   */
  public static function buildNdjsonExportBatch(array $responses, $survey, $truncate = FALSE) {
    $output = NULL;

    $questionMatrix = SurveyQuestion::generateQuestionMatrix($survey);

    foreach ($responses as $response) {
      $row = self::buildDataRow($response, $questionMatrix);

      $output .= json_encode($row) . "\n";

      \Drupal::database()->update('survey_response')
        ->fields([
          'sent_to_bq' => 1,
        ])
        ->condition('id', $response->id)
        ->execute();
    }

    /** @var \Drupal\simple_survey\Service\BigQueryConnect $bq */
    $bq = \Drupal::service('simple_survey.big_query');

    $simpleSurvey = SimpleSurvey::load($survey);

    $bq->setSurvey($simpleSurvey);

    $bq->sendJsonData($simpleSurvey->bigQueryDataset, $simpleSurvey->bigQueryTable, $output, $truncate);

  }

  /**
   * Builds an NDJSON string out from response IDs.
   *
   * @param array $responseIds
   *   Array of responses ids.
   *
   * @return mixed
   *   Null|NDJSON string.
   */
  public function buildNdjsonExport(array $responseIds) {
    $output = NULL;

    $responses = SurveyResponse::loadMultipleDb($responseIds);

    $questionMatrix = SurveyQuestion::generateQuestionMatrix($this->survey->id());

    foreach ($responses as $response) {
      $row = self::buildDataRow($response, $questionMatrix);

      $output .= json_encode($row) . "\n";
    }

    return $output;
  }

  /**
   * Creates a response data row.
   *
   * @param object $response
   *   The survey response.
   * @param array $questionMatrix
   *   The questions for the survey.
   *
   * @return array
   *   The data row.
   */
  public static function buildDataRow($response, array $questionMatrix) {
    $row = [];

    $row['drupal_id'] = $response->id;
    $row['cookie_uuid'] = $response->cookie_uuid;
    $row['site_path'] = $response->site_path;
    $row['langcode'] = $response->langcode;
    $row['created'] = \Drupal::service('date.formatter')->format($response
      ->created, 'custom', 'Y-m-d H:i:s');

    $survey_answers = SurveyAnswer::loadMultipleBatch($response->uuid);

    foreach ($survey_answers as $survey_answer) {
      $question = $questionMatrix[$survey_answer->question_id];
      $row[$question['name']] = $survey_answer->answer;
    }

    return $row;
  }

}