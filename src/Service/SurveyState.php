<?php

namespace Drupal\simple_survey\Service;

use Drupal\Core\Datetime\DateFormatter;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\State\StateInterface;

/**
 * Manages DataSource States.
 */
class SurveyState {
  use MessengerTrait;

  /**
   * The total number of FOAs.
   *
   * @var int
   */
  private $totalResponses = 0;

  /**
   * The module name.
   *
   * @var string
   */
  private $moduleName = 'simple_survey';

  /**
   * Drupal logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  /**
   * Date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * Date time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The State service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Service config and DI.
   *
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   Date formatter service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   * @param \Drupal\Drupal\Core\State\StateInterface $stateInterface
   *   The state service.
   * @param \Drupal\Component\Datetime\TimeInterface $timeInterface
   *   The time service.
   */
  public function __construct(
    DateFormatter $dateFormatter,
    LoggerChannelFactoryInterface $logger_factory,
    StateInterface $stateInterface,
    TimeInterface $timeInterface
  ) {
    $this->dateFormatter = $dateFormatter;
    $this->logger = $logger_factory->get($this->moduleName);
    $this->state = $stateInterface;
    $this->time = $timeInterface;
  }

  /**
   * Set last time survey was exported.
   *
   * @param string $survey
   *   The data source machine name.
   */
  public function setLastExported($survey) {
    $this->state->set($this->moduleName . '.' . $survey . '.lastExported', $this->time->getRequestTime());
  }

  /**
   * Set last imported state.
   *
   * @param string $survey
   *   The data source machine name.
   *
   * @return string
   *   Returns last imported date.
   */
  public function getLastExported($survey) {
    return $this->state->get($this->moduleName . '.' . $survey . '.lastExported');
  }

  /**
   * Set total records.
   *
   * @param string $survey
   *   The data source machine name.
   * @param int $totalRecords
   *   The total records in the source at that time.
   */
  public function setTotalRecords($survey, $totalRecords) {
    $this->state->set($this->moduleName . '.' . $survey . '.totalRecords', $totalRecords);
  }

  /**
   * Get total records.
   *
   * @param string $survey
   *   The data source machine name.
   *
   * @return int
   *   Returns total records created.
   */
  public function getTotalRecords($survey) {
    return !is_null($this->state->get($this->moduleName . '.' . $survey . '.totalRecords')) ?
    $this->state->get($this->moduleName . '.' . $survey . '.totalRecords') : 0;
  }

  /**
   * Set exported amount.
   *
   * @param string $survey
   *   The data source machine name.
   * @param int $totalRecords
   *   The total records created.
   */
  public function setRecordsExported($survey, $totalRecords) {
    $this->state->set($this->moduleName . '.' . $survey . '.exported', $totalRecords);
  }

  /**
   * Get exported amount.
   *
   * @param string $survey
   *   The data source machine name.
   *
   * @return int
   *   Returns number of records created.
   */
  public function getRecordsExported($survey) {
    return !is_null($this->state->get($this->moduleName . '.' . $survey . '.exported')) ?
    $this->state->get($this->moduleName . '.' . $survey . '.exported') : 0;
  }

  /**
   * Set current queue count.
   *
   * @param string $survey
   *   The data source machine name.
   * @param int $count
   *   The current queue count.
   */
  public function setQueueCount($survey, $count) {
    $this->state->set($this->moduleName . '.' . $survey . '.queueCount', $count);
  }

  /**
   * Get the current queue count.
   *
   * @param string $survey
   *   The data source machine name.
   *
   * @return int
   *   Returns current queue count
   */
  public function getQueueCount($survey) {
    return !is_null($this->state->get($this->moduleName . '.' . $survey . '.queueCount')) ?
    $this->state->get($this->moduleName . '.' . $survey . '.queueCount') : 0;
  }

}
