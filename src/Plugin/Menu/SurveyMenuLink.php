<?php

namespace Drupal\simple_survey\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;

class SurveyMenuLink extends MenuLinkDefault {}