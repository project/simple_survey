<?php

namespace Drupal\simple_survey\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Derivative class that provides the menu links for surveys.
 */
class SurveyMenuLink extends DeriverBase implements ContainerDeriverInterface {
  /**
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager service.
   */
  protected $entityTypeManager;

  /**
   * Creates a ProductMenuLink instance.
   *
   * @param string $base_plugin_id
   *   The plugin id.
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct($base_plugin_id, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];

    /** @var \Drupal\simple_survey\Entity\SimpleSurvey[] $surveys */
    $surveys = $this->entityTypeManager->getStorage('simple_survey')->loadMultiple();

    foreach ($surveys as $survey) {
      $links[$survey->id()] = [
        'title' => $survey->label(),
        'route_name' => 'entity.simple_survey.canonical',
        'parent' => 'entity.simple_survey.collection',
        'route_parameters' => [
          'simple_survey' => $survey->id(),
        ],
      ] + $base_plugin_definition;
    }

    return $links;
  }

}