<?php

namespace Drupal\entity_reference_quantity\Plugin\Field\FieldWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;

/**
 * @FieldWidget(
 *   id = "entity_reference_quantity_autocomplete",
 *   label = @Translation("Autocomplete"),
 *   description = @Translation("An autocomplete text field with associated data."),
 *   field_types = {
 *     "entity_reference_quantity"
 *   }
 * )
 */
class VisibilityPathAutocomplete extends EntityReferenceAutocompleteWidget {

  /**
   * {@inheritDoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $widget = [
      '#attributes' => ['class' => ['form--inline', 'clearfix']],
      '#theme_wrappers' => ['container'],
    ];

    $widget['target_id'] = parent::formElement($items, $delta, $element, $form, $form_state);

    $widget['visibility'] = [
      '#type' => 'radios',
      '#default_value' => isset($items[$delta]) ? $items[$delta]->visibility : 1,
      '#options' => [
        1 => 'Show survey on this page',
        0 => 'Hide survey on this page',
      ],
      '#weight' => 10,
    ];

    $widget['child_paths'] = [
      '#type' => 'radios',
      '#default_value' => isset($items[$delta]) ? $items[$delta]->visibility : 0,
      '#options' => [
        1 => 'Include paths below this page',
        0 => 'Only this page',
      ],
      '#weight' => 10,
    ];

    return $widget;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $values = parent::massageFormValues($values, $form, $form_state);
    foreach ($values as $delta => $data) {
      if (empty($data['visibility'])) {
        unset($values[$delta]['visibility']);
      }
      if (empty($data['child_paths'])) {
        unset($values[$delta]['child_paths']);
      }
    }
    return $values;
  }

}
