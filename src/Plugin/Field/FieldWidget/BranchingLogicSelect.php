<?php

namespace Drupal\simple_survey\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * @FieldWidget(
 *   id = "branching_logic_select",
 *   label = @Translation("Branching Logic Field Type Default Widget"),
 *   description = @Translation("Branching Logic Field Type Default Widget"),
 *   field_types = {
 *     "branching_logic",
 *   }
 * )
 */
class BranchingLogicSelect extends OptionsWidgetBase {

  /**
   * {@inheritDoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element += [
      '#type' => 'select',
      '#options' => $this->getOptions($items->getEntity()),
      '#default_value' => isset($items[$delta]->target_id) ? $items[$delta]->target_id : '',
    ];

    $element['#title'] = t('Destination Question');
    $element['#title_display'] = 'before';

    $widget = [
      '#attributes' => ['class' => ['form--inline', 'clearfix']],
      '#theme_wrappers' => ['container'],
    ];

    $widget['question_option'] = [
      '#type' => 'textfield',
      '#title' => t('Option Value'),
      '#placeholder' => t('Choose a selection option from the question'),
      '#default_value' => isset($items[$delta]->question_option) ? $items[$delta]->question_option : NULL,
      '#size' => 20
    ];

    $widget['target_id'] = $element;

    return $widget;
  }

  /**
   * {@inheritdoc}
   */
  protected function sanitizeLabel(&$label) {
    // Select form inputs allow unencoded HTML entities, but no HTML tags.
    $label = Html::decodeEntities(strip_tags($label));
  }

  /**
   * {@inheritdoc}
   */
  protected function getEmptyLabel() {
    // Single select: add a 'none' option for non-required fields,
    // and a 'select a value' option for required fields that do not come
    // with a value selected.
    if (!$this->required) {
      return t('- None -');
    }
    if (!$this->has_value) {
      return t('- Select a value -');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if ($element['#value'] == '_none') {
      if ($element['#required'] && $element['#value'] == '_none') {
        $form_state->setError($element, t('@name field is required.', ['@name' => $element['#title']]));
      }
      else {
        $form_state->setValueForElement($element, NULL);
      }
    }
    else {
      $form_state->setValueForElement($element, $element['#value']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $delta => $data) {
      if (isset($data['element'])) {
        $values[$delta] = $data['element'];
      }
      if (empty($data['question_option'])) {
        unset($values[$delta]['question_option']);
      }
    }
    return $values;
  }

}
