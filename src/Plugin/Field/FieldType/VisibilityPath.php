<?php

namespace Drupal\simple_survey\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Validation\Plugin\Validation\Constraint\AllowedValuesConstraint;

/**
 * Allows a user to select a page and then specify if its path and paths under it should be included for a survey.
 *
 * @FieldType(
 *   id = "visibility_path",
 *   module = "simple_survey",
 *   label = @Translation("visibility_path"),
 *   description = @Translation("Allows a user to select a page and then specify if its path and paths under it should be included for a survey."),
 *   default_widget = "visibility_path_autocomplete",
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList",
 * )
 */
class VisibilityPath extends EntityReferenceItem {

  /**
   * {@inheritDoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['visibility'] = DataDefinition::create('boolean')
      ->setLabel(t('Show or Hide'))
      ->setDescription(t('Whether this path should show or hide the survey.'))
      ->setRequired(TRUE);

    $properties['child_paths'] = DataDefinition::create('boolean')
      ->setLabel(t('Include child paths'))
      ->setDescription(t('Whether this paths below this one should also show the survey.'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'target_id';
  }

  /**
   * {@inheritDoc}
   */
  public function isEmpty() {
    return empty($this->values['visibility']);
  }

  /**
   * {@inheritDoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);

    $schema['columns']['visibility'] = [
      'type' => 'int',
      'not null' => TRUE,
      'default' => 1,
    ];

    $schema['columns']['child_paths'] = [
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = FieldItemBase::getConstraints();
    // Remove the 'AllowedValuesConstraint' validation constraint because entity
    // reference fields already use the 'ValidReference' constraint.
    foreach ($constraints as $key => $constraint) {
      if ($constraint instanceof AllowedValuesConstraint) {
        unset($constraints[$key]);
      }
    }
    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function getPreconfiguredOptions() {
    // In the base EntityReference class, this is used to populate the
    // list of field-types with options for each destination entity type.
    // Too much work, we'll just make people fill that out later.
    // Also, keeps the field type dropdown from getting too cluttered.
    return [];
  }

}
