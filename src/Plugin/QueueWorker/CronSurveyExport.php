<?php

namespace Drupal\simple_survey\Plugin\QueueWorker;

/**
 * Processes tasks for Survey queue.
 *
 * @QueueWorker(
 *   id = "simple_survey_responses_to_bq",
 *   title = @Translation("Export Responses to BigQuery"),
 *   cron = {"time" = 90}
 * )
 */
class CronSurveyExport extends SurveyResponseQueueWorkerBase {}
