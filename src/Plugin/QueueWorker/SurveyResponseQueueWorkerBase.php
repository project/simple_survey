<?php

namespace Drupal\simple_survey\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\simple_survey\Service\SurveyState;
use Drupal\Component\Datetime\TimeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\simple_survey\Service\BigQueryConnect;
use Drupal\simple_survey\Service\ResponseExportService;

/**
 * Processes tasks for survey response queue.
 */
class SurveyResponseQueueWorkerBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Survey state service.
   *
   * @var \Drupal\simple_survey\Service\ResponseExportService
   */
  private $responseExport;

  /**
   * Data source state service.
   *
   * @var \Drupal\simple_survey\Service\SurveyState
   */
  private $surveyState;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * Drupal logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  protected $logger;

  /**
   * The Entity Type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The BigQuery client.
   *
   * @var \Drupal\simple_survey\Service\BigQueryConnect
   */
  protected $bigQueryClient;

  /**
   * Queueworker Construct.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   * @param \Drupal\simple_survey\Service\SurveyState $surveyState
   *   Survey state service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Time service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager service.
   * @param \Drupal\simple_survey\Service\ResponseExportService $responseExportService
   *   Response export service.
   * @param \Drupal\simple_survey\Service\BigQueryConnect $bigQueryClient
   *   BigQuery client service.
   */
  public function __construct(
    LoggerChannelFactoryInterface $logger_factory,
    SurveyState $surveyState,
    TimeInterface $time,
    EntityTypeManagerInterface $entity_type_manager,
    ResponseExportService $responseExportService,
    BigQueryConnect $bigQueryClient) {
    $this->dataSourceState = $surveyState;
    $this->time = $time;
    $this->logger = $logger_factory->get('simple_survey');
    $this->entityTypeManager = $entity_type_manager;
    $this->responseExport = $responseExportService;
    $this->bigQueryClient = $bigQueryClient;
    $this->surveyState = $surveyState;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition) {
    return new static(
      $container->get('logger.factory'),
      $container->get('simple_survey.survey_state'),
      $container->get('datetime.time'),
      $container->get('entity_type.manager'),
      $container->get('simple_survey.response_export'),
      $container->get('simple_survey.big_query')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($item) {
    $survey = $this->entityTypeManager->getStorage('simple_survey')
      ->load($item['survey']);

    if ($survey && $this->bigQueryClient->checkConfigured()) {
      $this->bigQueryClient->setSurvey($survey);

      $this->responseExport->survey = $survey;

      $output = $this->responseExport->buildNdjsonExport($item['responseIds']);

      if (!empty($output) && !empty($survey->bigQueryDataset) && !empty($survey->bigQueryTable)) {
        $dataSent = $this->bigQueryClient->sendJsonData($survey->bigQueryDataset, $survey->bigQueryTable, $output);

        if ($dataSent) {
          $this->logger->info($this->t(':records queued responses sent to BigQuery for :survey.', [
            ':survey' => $survey->label(),
            ':records' => count($item['responseIds']),
          ]));

          $this->surveyState->setLastExported($survey->id());

          /** @var \Drupal\simple_survey\Entity\SurveyResponse[] $responses */
          $responses = $this->entityTypeManager->getStorage('survey_response')
            ->loadMultiple($item['responseIds']);

          foreach ($responses as $response) {
            $response->set('sent_to_bq', TRUE);
            $response->save();
          }
        }
      }
    }
  }

}
