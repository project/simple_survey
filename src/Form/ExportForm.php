<?php

namespace Drupal\simple_survey\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Form handler for the Example add and edit forms.
 */
class ExportForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\simple_survey\Entity\SimpleSurvey $simpleSurvey */
    $simpleSurvey = $this->entity;

    $simpleSurvey->checkStatuses();
    $date = new \DateTime();
    $date->sub(new \DateInterval('P1M'));

    $form['date_range'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Date Range'),
      '#weight' => 2,
    ];

    $form['date_range']['from_date'] = [
      '#type' => 'date',
      '#name' => 'from_date',
      '#title' => $this->t('Starting Date'),
      '#description' => $this->t('The date you want to start date you want to export from. Defaults to one month prior to the current day.'),
      '#default_value' => $date->format('Y-m-d'),
      '#required' => TRUE,
    ];

    $form['date_range']['to_date'] = [
      '#type' => 'date',
      '#name' => 'to_date',
      '#title' => $this->t('Ending Date'),
      '#description' => $this->t('The date you want to end date you want to export from. Defaults to today.'),
      '#default_value' => date('Y-m-d'),
    ];

    if (class_exists('Google\Cloud\BigQuery\BigQueryClient')) {

      $form['export_type'] = [
        '#type' => 'radios',
        '#title' => $this->t('Export Type'),
        '#description' => $this->t('Choose to send to BigQuery or export as CSV.'),
        '#default_value' => 'csv',
        '#options' => [
          'csv' => $this->t('Export as CSV'),
          'bq' => $this->t('Send to BigQuery'),
        ],
        '#weight' => 0,
      ];

      $form['sent_to_bq'] = [
        '#type' => 'checkbox',
        '#name' => 'sent_to_bq',
        '#title' => $this->t('Export Responses that have already been sent to BigQuery'),
        '#description' => $this->t('If checked, this will resend responses that have already been sent to BigQuery. This may result in duplicates in BigQuery.'),
        '#default_value' => FALSE,
        '#weight' => 1,
        '#states' => [
          'visible' => [
            [
              ':input[name="export_type"]' => ['value' => 'bq'],
            ],
          ],
        ],
      ];
    }
    else {
      $form['bigQueryTable'] = [
        '#type' => 'item',
        '#title' => $this->t('BigQuery Client for PHP not installed'),
        '#markup' => $this->t('Please install %bqlink to use this feature', [
          '%bqlink' => Link::fromTextAndUrl('Google BigQuery Client for PHP', Url::fromUri('https://packagist.org/packages/google/cloud-bigquery')),
        ]),
      ];
    }

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $startDate = strtotime($form_state->getValue('from_date'));
    $endDate = strtotime($form_state->getValue('to_date'));

    if ($endDate < $startDate) {
      $form_state->setErrorByName('to_date', $this->t('The end date :to_date cannot be before the start date :from_date.', [
        ':to_date' => $endDate,
        ':from_date' => $startDate,
      ]));
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\simple_survey\Entity\SimpleSurvey $simple_survey */
    $simple_survey = $this->entity;
    $options = [
      'survey' => $simple_survey->id(),
      'sent_to_bq' => $form_state->getValue('sent_to_bq'),
      'start_date' => $form_state->getValue('from_date'),
      'end_date' => $form_state->getValue('to_date'),
    ];

    if ($form_state->getValue('export_type') == 'csv') {
      $batch = $this->createCsvBatch($options);
    }
    else {
      $batch = $this->createBqBatch($options);
    }

    batch_set($batch);
  }

  private function createBqBatch(array $options) {
    /** @var \Drupal\simple_survey\Entity\SimpleSurvey $simple_survey */
    $simple_survey = $this->entity;

    $batch = [
      'title' => $this->t('Send Data to BigQuery for %survey', [
        '%survey' => $simple_survey->label(),
      ]),
      'operations' => [
        [
          '\Drupal\simple_survey\Service\ResponseExportService::sendResponses',
          [
            $options,
          ],
        ],
      ],
      'finished' => '\Drupal\simple_survey\Controller\BigQueryController::finishedProcessingBq',
      'init_message' => $this->t('Resending data to BigQuery for @source', [
        '@source' => $simple_survey->label(),
      ]),
      'progress_message' => $this->t('Time left: @estimate | Time elapsed: @elapsed', [
        '@source' => $simple_survey->label(),
      ]),
      'error_message' => $this->t('The export progress has encountered an error.'),
    ];

    return $batch;
  }

  private function createCsvBatch(array $options) {
    /** @var \Drupal\simple_survey\Entity\SimpleSurvey $simple_survey */
    $simple_survey = $this->entity;
    $date = new \DateTime();
    $fileTime = $date->format('YdmHis');
    $options['filename'] = $simple_survey->id() . '-survey-export-' . $fileTime . '.csv';

    $batch = [
      'title' => $this->t('Creating CSV export :filename for :survey', [
        ':survey' => $simple_survey->label(),
        ':filename' => $options['filename'],
      ]),
      'operations' => [
        [
          '\Drupal\simple_survey\Service\ResponseExportService::processOptionsData',
          [
            $options,
          ],
        ],
      ],
      'finished' => '\Drupal\simple_survey\Service\ResponseExportService::finishedFileProcessing',
      'progress_message' => $this->t('Time left: @estimate | Time elapsed: @elapsed', [
        '@source' => $simple_survey->label(),
      ]),
      'error_message' => $this->t('The export progress has encountered an error.'),
    ];

    return $batch;
  }

}
