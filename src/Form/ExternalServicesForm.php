<?php

namespace Drupal\simple_survey\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;

/**
 * Form handler for the Example add and edit forms.
 */
class ExternalServicesForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\simple_survey\Entity\SimpleSurvey $simpleSurvey */
    $simpleSurvey = $this->entity;

    $simpleSurvey->checkStatuses();

    if (class_exists('Google\Cloud\BigQuery\BigQueryClient')) {

      $form['bigQueryProjectId'] = [
        '#type' => 'textfield',
        '#title' => $this->t('BigQuery Project Id'),
        '#description' => $this->t('Project Id from Google Console'),
        '#default_value' => $simpleSurvey->get('bigQueryProjectId'),
        '#format' => 'plain_text',
        '#group' => 'simple_survey_data_storage',
      ];

      $form['bigQueryKeyfile'] = [
        '#type' => 'textarea',
        '#title' => $this->t('BigQuery Keyfile Text'),
        '#description' => $this->t('Keyfile provided from BigQuery'),
        '#default_value' => $simpleSurvey->get('bigQueryKeyfile'),
        '#group' => 'simple_survey_data_storage',
      ];

      $form['bigQueryDataset'] = [
        '#type' => 'textfield',
        '#title' => $this->t('BigQuery Dataset'),
        '#description' => $this->t('Dataset from Google Console'),
        '#default_value' => $simpleSurvey->get('bigQueryDataset'),
        '#format' => 'plain_text',
        '#group' => 'simple_survey_data_storage',
      ];

      $form['bigQueryTable'] = [
        '#type' => 'textfield',
        '#title' => $this->t('BigQuery Table'),
        '#description' => $this->t('Table from Google Console'),
        '#default_value' => $simpleSurvey->get('bigQueryTable'),
        '#format' => 'plain_text',
        '#group' => 'simple_survey_data_storage',
      ];
    }
    else {
      $form['bigQueryTable'] = [
        '#type' => 'item',
        '#title' => $this->t('BigQuery Client for PHP not installed'),
        '#markup' => $this->t('Please install %bqlink to use this feature', [
          '%bqlink' => Link::fromTextAndUrl('Google BigQuery Client for PHP', Url::fromUri('https://packagist.org/packages/google/cloud-bigquery')),
        ]),
        '#group' => 'simple_survey_data_storage',
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\simple_survey\Entity\SimpleSurvey $simpleSurvey */
    $simpleSurvey = $this->entity;

    $status = $simpleSurvey->save();

    if ($status) {
      $this->messenger()->addMessage($this->t('Saved the %label Survey.', [
        '%label' => $simpleSurvey->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label Survey was not saved.', [
        '%label' => $simpleSurvey->label(),
      ]), MessengerInterface::TYPE_ERROR);
    }

    $form_state->setRedirect('entity.simple_survey.external_services_form', [
      'simple_survey' => $simpleSurvey->id(),
    ]);
  }

}
