<?php

namespace Drupal\simple_survey\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Form handler for the Example add and edit forms.
 */
class SurveyDisplayForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\simple_survey\Entity\SimpleSurvey $simpleSurvey */
    $simpleSurvey = $this->entity;

    $simpleSurvey->checkStatuses();

    $form['anchorTag'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Where will the form display on page?'),
      '#description' => $this->t('Decides where the survey will display on page. <strong>Body</strong> will cause the form to be sticky to a position on page. Any other value will cause it to attach to the element. Use <strong>#</strong> for IDs and <strong>.</strong> for classes.'),
      '#default_value' => $simpleSurvey->get('anchorTag') ?: 'body',
    ];

    $form['attachStylesheet'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use internal form stylesheet'),
      '#description' => $this->t('Uses stylesheet that comes with Simple Survey'),
      '#default_value' => !is_null($simpleSurvey->get('attachStylesheet')) ?
      $simpleSurvey->get('attachStylesheet') : 1,
      '#options' => [
        1 => "Yes",
        0 => "No",
      ],
    ];

    $form['surveyWhenLoggedIn'] = [
      '#type' => 'checkbox',
      '#name' => 'surveyWhenLoggedIn',
      '#title' => $this->t('Show the survey when a user is logged in'),
      '#description' => $this->t('Shows the survey when a user is logged in'),
      '#default_value' => !is_null($simpleSurvey->get('surveyWhenLoggedIn')) ?
        $simpleSurvey->get('surveyWhenLoggedIn') : 1,
      '#options' => [
        1 => "Yes",
        0 => "No",
      ],
    ];

    $form['display_filtering'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Display Filtering'),
      '#description' => $this->t('Settings for how the survey appears.'),
    ];

    $form['display_filtering']['displayFilterByPath'] = [
      '#type' => 'textarea',
      '#name' => 'displayFilterByPath',
      '#title' => $this->t('Display survey only on the listed paths.'),
      '#description' => $this->t('Enter a new path on each line. To include sub-paths add an asterisk Example: <br>"&lt;front&gt;": front page<br>"/example": only this page<br>"/example/*": this page and pages below it<br>"!/example": not on this page'),
      '#default_value' => $simpleSurvey->get('displayFilterByPath'),
      '#format' => 'plain_text',
    ];

    $form['display_timing'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Display Timing'),
    ];

    $form['display_timing']['displayDelayType'] = [
      '#type' => 'select',
      '#name' => 'displayDelayType',
      '#title' => $this->t('Delay based on position or time'),
      '#description' => $this->t('Toggles the Survey tool delay between none, position or time. Default is based on time'),
      '#default_value' => $simpleSurvey->get('displayDelayType') ?: 'time',
      '#options' => [
        "time" => "Time",
        "position" => "Position",
        "none" => "None",
      ],
    ];

    $form['display_timing']['delayPosition'] = [
      '#type' => 'radios',
      '#title' => $this->t('Page Display Position'),
      '#description' => $this->t('Point at which the form should display if using the position based display. Default is bottom'),
      '#default_value' => $simpleSurvey->get('delayPosition') ?: 'bottom',
      '#options' => [
        "bottom" => "Bottom",
        "top" => "Top",
        "middle" => "Middle",
      ],
      '#name' => 'delayPosition',
      '#states' => [
        'visible' => [
          ':input[name="displayDelayType"]' => ['value' => 'position'],
        ],
      ],
    ];

    $form['display_timing']['delay'] = [
      '#type' => 'number',
      '#title' => $this->t('Form Time Delay'),
      '#description' => $this->t('Amount of time before form displays'),
      '#default_value' => !is_null($simpleSurvey->get('delay')) ?
      $simpleSurvey->get('delay') : 0,
      '#name' => 'delay',
      '#states' => [
        'visible' => [
          ':input[name="displayDelayType"]' => ['value' => 'time'],
        ],
      ],
    ];

    $form['display_timing']['closingDelay'] = [
      '#type' => 'number',
      '#title' => $this->t('Closing Time Delay'),
      '#description' => $this->t('Amount of time before form closes after completion'),
      '#default_value' => !is_null($simpleSurvey->get('closingDelay')) ?
      $simpleSurvey->get('closingDelay') : 7,
      '#name' => 'closingDelay',
    ];

    $form['display_timing']['displayCooldown'] = [
      '#type' => 'number',
      '#title' => $this->t('Default Survey Display Cooldown'),
      '#description' => $this->t('Amount of time before form will display after being closed. Minimum 1 day.'),
      '#default_value' => !is_null($simpleSurvey->get('displayCooldown')) ?
      $simpleSurvey->get('displayCooldown') : 1,
      '#name' => 'displayCooldown',
    ];

    $form['display_timing']['answeredDisplayCooldown'] = [
      '#type' => 'number',
      '#title' => $this->t('Answered Survey Display Cooldown'),
      '#description' => $this->t('Amount of time before form will display after being submitted in days. Minimum 1 day.'),
      '#default_value' => !is_null($simpleSurvey->get('answeredDisplayCooldown')) ?
      $simpleSurvey->get('answeredDisplayCooldown') : 1,
      '#name' => 'answeredDisplayCooldown',
    ];

    $form['useFontAwesome'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use FontAwesome'),
      '#description' => $this->t('If the site has font awesome available, use it'),
      '#default_value' => !is_null($simpleSurvey->get('useFontAwesome')) ?
      $simpleSurvey->get('useFontAwesome') : 0,
      '#options' => [
        1 => "Yes",
        0 => "No",
      ],
      '#name' => 'useFontAwesome',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\simple_survey\Entity\SimpleSurvey $simpleSurvey */
    $simpleSurvey = $this->entity;

    $status = $simpleSurvey->save();

    if ($status) {
      $this->messenger()->addMessage($this->t('Saved the %label Survey.', [
        '%label' => $simpleSurvey->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label Survey was not saved.', [
        '%label' => $simpleSurvey->label(),
      ]), MessengerInterface::TYPE_ERROR);
    }

    $form_state->setRedirect('entity.simple_survey.display_form', [
      'simple_survey' => $simpleSurvey->id(),
    ]);
  }

}
