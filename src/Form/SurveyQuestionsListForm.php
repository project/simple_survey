<?php

namespace Drupal\simple_survey\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\simple_survey\Entity\SurveyQuestion;

/**
 * Form handler for the Example add and edit forms.
 */
class SurveyQuestionsListForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $questionTypes = [
      'normal' => 'Question display',
      'informational' => 'Informational display',
      'closing' => 'Closing display',
    ];

    /** @var \Drupal\simple_survey\Entity\SimpleSurvey $simpleSurvey */
    $simpleSurvey = $this->entity;

    $simpleSurvey->checkStatuses();

    if (!$simpleSurvey->isNew()) {
      $form['info'] = [
        '#type' => 'item',
        '#title' => 'Edit questions and select the starting question',
        '#markup' => $this->t('<p>To select a starting question, please choose one from the options below.</p>'),
      ];

      $form['startingQuestion'] = [
        '#type' => 'table',
        '#header' => [
          $this->t('Title'),
          $this->t('Question Type'),
          $this->t('Operations'),
        ],
        '#empty' => $this->t('There are no questions yet. <a href="@add-url">Add a question.</a>', [
          '@add-url' => Url::fromRoute('entity.simple_survey.survey_question.add_form', [
            'simple_survey' => $simpleSurvey->id(),
          ])->toString(),
        ]),
        '#tableselect' => TRUE,
        '#multiple' => FALSE,
        '#js_select' => FALSE,
        '#default_value' => $simpleSurvey->get('startingQuestion'),
      ];

      $query = $this->entityTypeManager->getStorage('survey_question')->getQuery();

      $sqids = $query->condition('survey', $simpleSurvey->id())
        ->execute();

      $survey_questions = SurveyQuestion::loadMultiple($sqids);

      if ($simpleSurvey->get('startingQuestion') == NULL) {
        $this->messenger()->addMessage($this->t('The %label survey does not have a starting question set.', [
          '%label' => $simpleSurvey->label(),
        ]), MessengerInterface::TYPE_ERROR);
      }

      if (!$simpleSurvey->hasClosingQuestion()) {
        $this->messenger()->addMessage($this->t('The %label survey does not have a closing question set.', [
          '%label' => $simpleSurvey->label(),
        ]), MessengerInterface::TYPE_ERROR);
      }

      foreach ($survey_questions as $id => $survey_question) {
        // Some table columns containing raw markup.
        $form['startingQuestion'][$id]['title'] = [
          '#markup' => $survey_question->question->value,
        ];

        // Some table columns containing raw markup.
        $form['startingQuestion'][$id]['question_type'] = [
          '#plain_text' => $questionTypes[$survey_question->question_type->value],
        ];

        // Operations (dropbutton) column.
        $form['startingQuestion'][$id]['operations'] = [
          '#type' => 'operations',
          '#links' => [],
        ];

        $form['startingQuestion'][$id]['operations']['#links']['edit'] = [
          'title' => $this->t('Edit'),
          'url' => Url::fromRoute('entity.simple_survey.survey_question.edit_form', [
            'survey_question' => $id,
            'simple_survey' => $simpleSurvey->id(),
          ]),
        ];

        $form['startingQuestion'][$id]['operations']['#links']['delete'] = [
          'title' => $this->t('Delete'),
          'url' => Url::fromRoute('entity.survey_question.delete_form', [
            'survey_question' => $id,
          ]),
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\simple_survey\Entity\SimpleSurvey $simpleSurvey */
    $simpleSurvey = $this->entity;

    $status = $simpleSurvey->save();

    if ($status) {
      $this->messenger()->addMessage($this->t('Saved the %label Survey.', [
        '%label' => $simpleSurvey->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label Survey was not saved.', [
        '%label' => $simpleSurvey->label(),
      ]), MessengerInterface::TYPE_ERROR);
    }

    $form_state->setRedirect('entity.simple_survey.questions_form', [
      'simple_survey' => $simpleSurvey->id(),
    ]);
  }

}
