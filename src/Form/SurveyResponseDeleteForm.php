<?php

namespace Drupal\feedbackjs\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Survey Response entities.
 *
 * @ingroup feedbackjs
 */
class SurveyResponseDeleteForm extends ContentEntityDeleteForm {


}
