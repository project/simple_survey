<?php

namespace Drupal\simple_survey\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * CSV Export Confirmation form.
 */
class ConfirmCsvExportForm extends EntityConfirmFormBase {


  /**
   * Drupal date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The Drupal Time Service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a EntityForm object.
   *
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   The date formatter service.
   */
  public function __construct(
    TimeInterface $time,
    DateFormatter $dateFormatter
  ) {
    $this->time = $time;
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('datetime.time'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to export the responses for @survey to CSV.', [
      '@survey' => $this->entity->label(),
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $fileTime = $this->dateFormatter->format($this->time->getRequestTime(), 'custom', 'YdmHis');
    $filename = $this->entity->id() . '-survey-export-' . $fileTime . '.csv';
    $survey = $this->entity->id();

    $batch = [
      'title' => $this->t('Creating CSV export :filename for :survey', [
        ':survey' => $this->entity->label(),
        ':filename' => $filename,
      ]),
      'operations' => [
        [
          '\Drupal\simple_survey\Service\ResponseExportService::processData',
          [
            $filename,
            $survey,
          ],
        ],
      ],
      'finished' => '\Drupal\simple_survey\Service\ResponseExportService::finishedFileProcessing',
      'progress_message' => $this->t('Time left: @estimate | Time elapsed: @elapsed', [
        '@source' => $this->entity->label(),
      ]),
      'error_message' => $this->t('The export progress has encountered an error.'),
    ];

    batch_set($batch);

    $form_state->setRedirect('entity.simple_survey.edit_form', [
      'simple_survey' => $survey,
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('entity.simple_survey.edit_form', [
      'simple_survey' => $this->entity->id(),
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function getConfirmText() {
    return $this->t('Export to CSV');
  }

  /**
   * {@inheritDoc}
   */
  public function getDescription() {
    $message = $this->t('You are exporting responses to CSV for @survey. Do you wish to continue?', [
      '@survey' => $this->entity->label(),
    ]);

    return $message;
  }

}
