<?php

namespace Drupal\simple_survey\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;

/**
 * Form handler for the Example add and edit forms.
 */
class SimpleSurveyForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\simple_survey\Entity\SimpleSurvey $simpleSurvey */
    $simpleSurvey = $this->entity;

    $simpleSurvey->checkStatuses();

    $form['label'] = [
      '#type' => 'textfield',
      '#name' => 'label',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $simpleSurvey->label(),
      '#description' => $this->t("Label for the Survey."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#name' => 'id',
      '#default_value' => $simpleSurvey->id(),
      '#machine_name' => [
        'exists' => '\Drupal\simple_survey\Entity\SimpleSurvey::load',
        'source' => ['label'],
      ],
    ];

    $form['description'] = [
      '#title' => $this->t('Description'),
      '#name' => 'description',
      '#type' => 'textarea',
      '#default_value' => $simpleSurvey->description(),
      '#group' => 'simple_survey_info',
    ];

    $form['survey_status'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Survey Status'),
      '#description' => $this->t('Settings for survey status. Displays some status info.'),
    ];

    $form['survey_status']['debug'] = [
      '#type' => 'checkbox',
      '#name' => 'debug',
      '#title' => $this->t('Enable Debug Mode'),
      '#description' => $this->t('Form will ignore delay'),
      '#default_value' => !is_null($simpleSurvey->getDebug()) ?
      $simpleSurvey->getDebug() : 0,
      '#options' => [
        1 => "Yes",
        0 => "No",
      ],
    ];

    $form['survey_status']['enableSurvey'] = [
      '#type' => 'checkbox',
      '#name' => 'enableSurvey',
      '#title' => $this->t('Enable survey'),
      '#description' => $this->t('Makes the survey active.'),
      '#default_value' => !is_null($simpleSurvey->get('enableSurvey')) ?
      $simpleSurvey->get('enableSurvey') : 0,
      '#options' => [
        1 => "Yes",
        0 => "No",
      ],
    ];

    $form['survey_status']['numberOfResponsesToStopAt'] = [
      '#type' => 'number',
      '#name' => 'numberOfResponsesToStopAt',
      '#title' => $this->t('Number of responses the survey should collect.'),
      '#description' => $this->t('Number of responses the survey should collect.'),
      '#default_value' => !is_null($simpleSurvey->get('numberOfResponsesToStopAt')) ?
      $simpleSurvey->get('numberOfResponsesToStopAt') : 0,
    ];

    $form['survey_status']['surveyActiveUntil'] = [
      '#type' => 'date',
      '#name' => 'surveyActiveUntil',
      '#title' => $this->t('Survey end date'),
      '#description' => $this->t('Date the survey should run until. The survey will end at the end of the day.'),
      '#default_value' => $simpleSurvey->get('surveyActiveUntil'),
    ];

    $form['privacy'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Privacy and Clearance'),
      '#description' => $this->t('Settings for Privacy policy and clearance.'),
    ];

    $form['privacy']['ombDisclaimer'] = [
      '#type' => 'checkbox',
      '#name' => 'ombDisclaimer',
      '#title' => $this->t('Enable OMB Disclaimer'),
      '#description' => $this->t('Will display OMB link.'),
      '#default_value' => !is_null($simpleSurvey->get('ombDisclaimer')) ?
      $simpleSurvey->get('ombDisclaimer') : 0,
      '#options' => [
        1 => "Yes",
        0 => "No",
      ],
    ];

    $form['privacy']['ombExpiration'] = [
      '#type' => 'date',
      '#name' => 'ombExpiration',
      '#title' => $this->t('OMB Expiration'),
      '#description' => $this->t('Date OMB clearance expires.'),
      '#default_value' => $simpleSurvey->get('ombExpiration'),
    ];

    $url = Url::fromUri('https://www.php.net/manual/en/datetime.format.php');

    $form['privacy']['ombExpirationFormat'] = [
      '#type' => 'textfield',
      '#name' => 'ombExpirationFormat',
      '#title' => $this->t('OMB Expiration Date Format'),
      '#description' => $this->t('Field expects conventions found at @link. Example "m-d-Y" will display MM-DD-YYYY', [
        '@link' => Link::fromTextAndUrl('PHP Date Formats', $url)->toString(),
      ]),
      '#default_value' => !is_null($simpleSurvey->get('ombExpirationFormat')) ?
      $simpleSurvey->get('ombExpirationFormat') : 'Y-m-d',
    ];

    $form['privacy']['ombId'] = [
      '#type' => 'textfield',
      '#name' => 'ombId',
      '#title' => $this->t('OMB ID'),
      '#description' => $this->t('OMB clearance ID.'),
      '#default_value' => $simpleSurvey->get('ombId'),
    ];

    $form['privacy']['ombLink'] = [
      '#type' => 'textfield',
      '#name' => 'ombLink',
      '#title' => $this->t('Link to OMB Disclaimer'),
      '#default_value' => $simpleSurvey->get('ombLink'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    /** @var \Drupal\simple_survey\Entity\SimpleSurvey $simpleSurvey */
    $simpleSurvey = $this->entity;

    $simpleSurvey->set('basePath', $this->t('/simple-survey/api/@simple_survey', [
      '@simple_survey' => $simpleSurvey->id(),
    ]));

    $simpleSurvey->set('questionsPath', Url::fromRoute('simple_survey.survey_questions', [
      'simple_survey' => $simpleSurvey->id(),
    ])->toString());

    $simpleSurvey->set('submitPath', Url::fromRoute('simple_survey.survey_submit', [
      'simple_survey' => $simpleSurvey->id(),
    ])->toString());

    $status = $simpleSurvey->save();

    if ($status) {
      $this->messenger()->addMessage($this->t('Saved the %label Survey.', [
        '%label' => $simpleSurvey->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label Survey was not saved.', [
        '%label' => $simpleSurvey->label(),
      ]), MessengerInterface::TYPE_ERROR);
    }

    $form_state->setRedirect('entity.simple_survey.edit_form', [
      'simple_survey' => $simpleSurvey->id(),
    ]);
  }

}
