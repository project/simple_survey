<?php

namespace Drupal\simple_survey\Form;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\simple_survey\Entity\SimpleSurvey;

/**
 * Class ContentEntityExampleSettingsForm.
 *
 * @package Drupal\simple_survey\Form
 *
 * @ingroup simple_survey
 */
class SurveyQuestionForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->entity;

    $form_state->setRedirect('entity.simple_survey.questions_form', [
      'simple_survey' => $entity->survey->entity->id(),
    ]);

    $status = $this->entity->save($form, $form_state);

    $message = $this->t('The survey question %feed has been added.', [
      '%feed' => $entity->get('question')->getValue(),
    ]);

    if ($status == SAVED_UPDATED) {
      $message = $this->t('The survey question %feed has been updated.', [
        '%feed' => $entity->get('question')->getValue(),
      ]);
    }

    $this->messenger()->addStatus($message);

    return $status;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SimpleSurvey $simple_survey = NULL) {
    $form = parent::buildForm($form, $form_state);

    $survey_question = $this->entity;

    $form['#title'] = $this->t('%simple_survey_label - %survey_question', [
      '%simple_survey_label' => $simple_survey->label(),
      '%survey_question' => Xss::filter($survey_question->question->value),
    ]);

    $form['question_entry'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Question Entry'),
    ];

    $form['input_configuration'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Input Configuration'),
      '#states' => [
        'visible' => [
          ':input[name="question_type"]' => ['value' => 'normal'],
        ],
      ],
    ];

    $form['question']['#group'] = 'question_entry';
    $form['question_disclaimer']['#group'] = 'question_entry';
    $form['question_type']['#group'] = 'question_entry';
    $form['next_question']['#group'] = 'question_entry';

    $form['input_type']['#group'] = 'input_configuration';
    $form['input_type']['#states'] = [
      'enabled' => [
        ':input[name="question_type"]' => ['value' => 'normal'],
      ],
    ];

    $form['display_as']['#group'] = 'input_configuration';
    $form['display_as']['#states'] = [
      'enabled' => [
        ':input[name="question_type"]' => ['value' => 'normal'],
      ],
      'visible' => [
        [
          ':input[name="input_type"]' => ['value' => 'checkbox'],
        ],
        [
          ':input[name="input_type"]' => ['value' => 'radio'],
        ],
      ],
    ];

    $form['placeholder_text']['#group'] = 'input_configuration';
    $form['placeholder_text']['#states'] = [
      'enabled' => [
        ':input[name="question_type"]' => ['value' => 'normal'],
      ],
      'visible' => [
        [
          ':input[name="input_type"]' => ['value' => 'textArea'],
        ],
        [
          ':input[name="input_type"]' => ['value' => 'text'],
        ],
      ],
    ];

    $form['options']['#group'] = 'input_configuration';
    $form['options']['#states'] = [
      'enabled' => [
        ':input[name="question_type"]' => ['value' => 'normal'],
      ],
      'visible' => [
        [
          ':input[name="input_type"]' => ['value' => 'checkbox'],
        ],
        [
          ':input[name="input_type"]' => ['value' => 'radio'],
        ],
        [
          ':input[name="input_type"]' => ['value' => 'select'],
        ],
      ],
    ];

    $form['branching']['#name'] = 'branching';
    $form['branching']['#group'] = 'input_configuration';
    $form['branching']['#states'] = [
      'enabled' => [
        ':input[name="question_type"]' => ['value' => 'normal'],
      ],
      'visible' => [
        [
          ':input[name="input_type"]' => ['value' => 'checkbox'],
        ],
        [
          ':input[name="input_type"]' => ['value' => 'radio'],
        ],
        [
          ':input[name="input_type"]' => ['value' => 'select'],
        ],
      ],
    ];

    $form['survey']['#disabled'] = TRUE;

    if ($this->entity->isNew()) {
      $form['survey']['widget']['#default_value'] = $simple_survey->id();
    }

    return $form;
  }

}
