<?php

namespace Drupal\simple_survey\Form;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Builds the form to delete a Simple Survey.
 */
class SurveyQuestionDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name?', [
      '%name' => Xss::filter($this->entity->get('question')->value),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.simple_survey.edit_form', [
      'simple_survey' => $this->entity->get('survey')->entity->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();
    $this->messenger()->addMessage($this->t('%label has been deleted.', [
      '%label' => Xss::filter($this->entity->get('question')->value),
    ]));

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
