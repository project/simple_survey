<?php

namespace Drupal\simple_survey\Commands;

use Symfony\Component\Console\Output\ConsoleOutput;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
class SimpleSurveyCommands extends DrushCommands {

  /**
   * Echos back hello with the argument provided.
   *
   * @param string $surveyId
   *   Argument provided to the drush command.
   *
   * @command simple_survey:sendResponsesBQ
   * @aliases ss:srbq
   * @options arr An option that takes multiple values.
   * @usage simple_survey:sendResponsesBQ default
   *   Sends the default survey response to BigQuery.
   */
  public function sendResponsesToBigQuery($surveyId = NULL) {

    $output = new ConsoleOutput();

    $output->writeln('<info>This command is no longer available</info>');
  }

  /**
   * Creates csv files for all or a single survey.
   *
   * @param string $surveyId
   *   Argument provided to the drush command.
   *
   * @command simple_survey:exportToCsv
   * @aliases ss:ec
   * @options arr An option that takes multiple values.
   * @usage simple_survey:sendResponsesBQ default
   *   Sends the default survey response to BigQuery.
   */
  public function exportToCsv($surveyId = NULL) {
    $output = new ConsoleOutput();

    $output->writeln('<info>This command is no longer available</info>');
  }

}
