<?php

namespace Drupal\simple_survey;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Survey Response entities.
 */
class SurveyAnswerViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();
    $baseTable = $this->entityType->getBaseTable() ?: $this->entityType->id();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    $data[$baseTable]['table']['join']['survey_response'] = [
      'left_field' => 'uuid',
      'field' => 'parent_uuid',
      'type' => 'INNER',
    ];

    return $data;
  }

}
