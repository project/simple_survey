<?php

use Symfony\Component\Console\Output\ConsoleOutput;
use Drupal\simple_survey\Entity\SimpleSurvey;

/**
 * Implements hook_drush_command().
 */
function simple_survey_drush_command() {
  $commands = [];

  if (class_exists('Google\Cloud\BigQuery\BigQueryClient')) {
    $commands['send-responses-to-bigquery'] = [
      'description' => 'Send survey tool responses to Big Query',
      'drupal dependencies' => ['simple_survey'],
      'aliases' => ['ss:srbq']
    ];

    $commands['export-to-bigquery'] = [
      'description' => 'Send survey tool responses to Big Query',
      'drupal dependencies' => ['simple_survey'],
      'aliases' => ['ss:ec']
    ];
  }

  return $commands;
}

/**
 * Call back function drush_custom_drush_command_send_responses_to_bigquery()
 * The call back function name in the  following format
 *   drush_{module_name}_{item_id_for_command}()
 */
function drush_simple_survey_send_responses_to_bigquery() {

  $output = new ConsoleOutput();

  $output->writeln('<info>This command no longer exist.</info>');
}

/**
 * Creates csv files for all or a single survey.
 *
 * @param string $surveyId
 *   Argument provided to the drush command.
 *
 * @command simple_survey:exportToCsv
 * @aliases ss:ec
 * @options arr An option that takes multiple values.
 * @usage export-to-bigquery default
 *   Sends the default survey response to BigQuery.
 */
function drush_simple_survey_export_to_bigquery($surveyId = NULL) {
  $output = new ConsoleOutput();

  $output->writeln('<info>This command no longer exist.</info>');
}
