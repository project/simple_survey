<?php

use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\simple_survey\Entity\SimpleSurvey;

/**
 * Implements hook_help().
 */
function simple_survey_help($route_name, RouteMatchInterface $route_match) {
  $helpLink = Link::createFromRoute(
          'Simple Feedback Survey help page',
          'help.page', [
            'name' => 'simple_survey',
          ])->toString();

  switch ($route_name) {
    case 'help.page.simple_survey':
      $output = '';
      $output .= '<h2>' . t('About') . '</h2>';
      $output .= '<p>' . t('The Simple Feedback Survey module allows you to
      create feed back surveys that collects customer experience data from users
      visiting your site. Currently you can collect data from any node content
      type on a site. This can be broken down to specific pages or pages under
      a specific path.') . '</p>';
      $output .= '<h2>' . t('Uses') . '</h2>';
      $output .= '<dl>';
      $output .= '<dt><strong>' . t('Adding a survey') . '</strong></dt>';
      $output .= '<dd>' . t('To add a survey go to %addsurveylink.', [
        '%addsurveylink' => Link::createFromRoute(
          'Add a simple survey',
          'entity.simple_survey.add_form')->toString(),
      ]) . '</dd>';
      $output .= '<dt><strong>' . t('Editing a survey') . '</strong></dt>';
      $output .= '<dd>' . t('To edit a survey go to %editsurveylink and select
      <em>Edit</em> from the operations drop down.', [
        '%editsurveylink' => Link::createFromRoute(
          'Simple Feedback Survey list',
          'entity.simple_survey.collection'
        )->toString(),
      ]) . '</dd>';
      $output .= '<dt><strong>' . t('Adding/Editing a survey: General Info') . '</strong></dt>';
      $output .= '<dd>' . t('The <strong>General Info</strong> section manages
      generic configuration for a survey. This includes:
        <ul>
        <li><em>Machine Name</em></li>
        <li><em>Description</em></li>
        <li><em>Survey Status</em> settings</li>
        <li><em>Privacy and Clearance</em> settings</li>
        <li><em>Survey API Info</em>. While this section cannot be modified, it
        provides useful info for developers who may want to use the module API
        directly.</li>
        </ul> All form fields are available while editing or adding in this section.') . '</dd>';
      $output .= '<dt><strong>' . t('Adding/Editing a survey: Display Settings') . '</strong></dt>';
      $output .= '<dd>' . t('The <strong>Display Settings</strong> section manages
      where and how the survey displays on the site. The following configuration
      can be changed:
        <ul>
        <li><em>Where will the form display on page?</em></li>
        <li><em>Display survey only on the listed paths.</em></li>
        <li><em>Display positioning/timing</em> settings</li>
        </ul> All form fields are available while editing or adding in this section.') . '</dd>';
      $output .= '<dt><strong>' . t('Adding/Editing a survey: Questions') . '</strong></dt>';
      $output .= '<dd>' . t('The <strong>Questions</strong> section manages
      what questions display on the survey and in what order. Currently, questions
      cannot be edited before the survey is saved. There are a few more restrictions:
        <ul>
        <li>Questions need to be added before the order can be set</li>
        <li>Checkboxes cannot be buttons. Future updates will disable the ability
        to have these selected together.</li>
        <li>The editing interface for questions and the question list is a bit clunky,
        this will be improved over time.</li>
        </ul>') . '</dd>';
      $output .= '<dt><strong>' . t('Adding/Editing a survey: Data Storage Settings') . '</strong></dt>';
      $output .= '<dd>' . t('The <strong>Data Storage Settings</strong> section manages
      external data storage. Currently this system only uses the BigQuery API.
      Internal storage is handled by the local db and can be exported out as CSV') . '</dd>';
      $output .= '</dl>';

      return $output;

    case 'entity.simple_survey.collection':
      $output = '';
      $output .= '<h2>' . t('About') . '</h2>';
      $output .= '<p>' . t('Here you can add different surveys to a site and
      manage those surveys. Currently only one survey can be active on a site,
      but that will change in future versions For more information visit the
      %helppage', [
        '%helppage' => $helpLink,
      ]) . '</p>';

      return $output;

    case 'entity.simple_survey.add_form':
    case 'entity.simple_survey.edit_form':
      $output = '';
      $output .= '<h2>' . t('Adding/editing a survey') . '</h2>';
      $output .= '<p>' . t('The configuration for a survey can be changed here.
      The configuration has four sections: <strong>General Info</strong>,
      <strong>Questions</strong>, <strong>Display Settings</strong>, and
      <strong>Data Storage Settings</strong>. For more information visit the
      %helppage', [
        '%helppage' => $helpLink,
      ]) . '</p>';

      return $output;
  }

}

/**
 * Implements hook_cron().
 */
function simple_survey_cron() {
  simple_survey_queue_bq_submissions();
}

function simple_survey_queue_bq_submissions() {
  /** @var \Drupal\simple_survey\Service\SurveyState $surveyState */
  $surveyState = \Drupal::service('simple_survey.survey_state');
  $queueFactory = \Drupal::service('queue');
  $entityTypeManager = \Drupal::entityTypeManager();
  $queue = $queueFactory->get('simple_survey_responses_to_bq');
  $surveys = SimpleSurvey::loadMultiple();
  $logger = \Drupal::logger('simple_survey');

  if (!class_exists('Google\Cloud\BigQuery\BigQueryClient')) {
    return;
  }

  /** @var \Drupal\simple_survey\Service\BigQueryConnect $bigQuery */
  $bigQuery = \Drupal::service('simple_survey.big_query');

  foreach ($surveys as $survey) {
    $bigQuery->setSurvey($survey);

    if (!$bigQuery->checkConfigured()) {
      continue;
    }

    if ($survey->isSurveyActive()) {
      $lastExported = !empty($surveyState->getLastExported($survey->id())) ?
        $surveyState->getLastExported($survey->id()) : \Drupal::time()->getRequestTime() - 86400;

      $responseIds = $entityTypeManager->getStorage('survey_response')->getQuery()
      ->condition('created', $lastExported, '>')
      ->condition('sent_to_bq', FALSE)
      ->condition('survey', $survey->id())
      ->execute();

      if (!empty($responseIds)) {
        $item = [
          'responseIds' => [],
          'survey' => $survey->id(),
        ];

        foreach ($responseIds as $responseId) {
          $item['responseIds'][] = (int) $responseId;
        }

        $queue->createItem($item);

        $logger->info(t(':count responses queued for :survey', [
          ':survey' => $survey->label(),
          ':count' => count($responseIds),
        ]));
      }
    }
  }
}

/**
 * Implements hook_page_attachments().
 */
function simple_survey_page_attachments(array &$attachments) {
  /** @var \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter */
  $dateFormatter = \Drupal::service('date.formatter');
  /** @var \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler */
  $moduleHandler = \Drupal::service('module_handler');

  if (!\Drupal::service('router.admin_context')->isAdminRoute()) {
    $survey = SimpleSurvey::getSurvey();

    if ($survey) {
      $omb_expiration_date = $survey->get('ombExpiration');

      if ($omb_expiration_date) {
        $date_format = $survey->get('ombExpirationFormat') ?
          $survey->get('ombExpirationFormat') : 'Y-m-d';
        $omb_expiration_date = $dateFormatter
          ->format(strtotime($omb_expiration_date), 'custom', $date_format);
      }

      $configOutput = [
        "parentTag" => $survey->get('anchorTag'),
        "language" => $survey->get('langcode') ?: 'en',
        "attachStylesheet" => $survey->get('attachStylesheet'),
        "debug" => $survey->get('debug'),
        "delay" => $survey->get('delay'),
        "configDelay" => $survey->get('closingDelay'),
        "closingDelay" => $survey->get('closingDelay'),
        "displayDelayType" => $survey->get('displayDelayType'),
        "configDelayPosition" => $survey->get('delayPosition'),
        "basePath" => $survey->get('basePath'),
        "questionsPath" => $survey->get('questionsPath'),
        "submitPath" => $survey->get('submitPath'),
        "validationPath" => $survey->get('validationPath'),
        "translations" => $survey->get('translations') == 1,
        "useFontAwesome" => $survey->get('useFontAwesome') == 1,
        "ombDisclaimer" => $survey->get('ombDisclaimer') == 1,
        "ombId" => $survey->get('ombId'),
        "ombExpiration" => $omb_expiration_date,
        "ombLink" => $survey->get('ombLink'),
        "startingQuestion" => $survey->get('startingQuestion'),
        "survey" => $survey->id(),
        "displayCooldown" => $survey->get('displayCooldown') ?: 1,
        "answeredDisplayCooldown" => $survey->get('answeredDisplayCooldown') ?: 1,
        "modulePath" => '/' . $moduleHandler->getModule('simple_survey')->getPath(),
      ];

      $attachments['#attached']['library'][] = 'simple_survey/simple_survey';
      $attachments['#attached']['drupalSettings']['simple_survey'] = $configOutput;
    }
  }
}

/**
 * Implements hook_entity_operation().
 *
 * @inheritDoc
 */
function simple_survey_entity_operation($entity) {
  $operations = [];

  if ($entity->getEntityTypeId() == 'simple_survey') {
    $operations['add_survey_question'] = [
      'title' => t('Add Survey Question'),
      'url' => Url::fromRoute('entity.simple_survey.survey_question.add_form', [
        'simple_survey' => $entity->id(),
      ]),
      'weight' => 50,
    ];
    $operations['export_to_csv'] = [
      'title' => t('Export Responses to CSV'),
      'url' => Url::fromRoute('entity.simple_survey.export_csv', [
        'simple_survey' => $entity->id(),
      ]),
      'weight' => 51,
    ];
  }

  return $operations;
}

/**
 * Implements hook_theme().
 */
function simple_survey_theme() {
  return [
    'simple_survey' => [
      'variables' => [
        'questionsPath' => NULL,
        'submitPath' => NULL,
        'lastExported' => NULL,
      ],
    ],
  ];
}
