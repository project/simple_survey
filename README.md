# Simple Feedback Survey

Simple Feedback Survey is simple customer initiated survey used primarily to collect customer experience data from users visiting your site.

How the survey is used:

1. Once the Drupal module is enabled, it should be configured based on the site and how it 
   should be displayed:
        - The survey can be sticky to the site or inline to content.
        - The CSS for the module can be overridden
2. The survey stores data in it’s own database table, however it can send data to BigQuery 
   which can be used with Datastudio.


## Requirements

- This module uses banbuilder for profanity checks.
  [Banbuilder](http://banbuilder.com/).

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.

## Known issues

In order for this module to fully function in Drupal 8.7 or below 
[this patch](https://www.drupal.org/files/issues/2019-10-14/2174633-351.patch) needs to be applied to drupal core. It is in reference to this issue [View output is not used for entityreference options](https://www.drupal.org/project/drupal/issues/2174633).

## Maintainers

- whurley - [whurley](https://www.drupal.org/user/430765)
- Matthew Clewley - [mclewley](https://www.drupal.org/u/mclewley)